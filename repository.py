import logging
from datetime import datetime, timedelta

from telebot import util

logger = logging.getLogger('SQL_Bot')
import json
import os
import sys
from functools import wraps
from time import sleep

import telebot
from sqlalchemy.engine import Engine
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import sessionmaker, scoped_session
# from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm.session import Session as sql_Session
from telebot.apihelper import ApiException
from telebot.types import InputMedia, Message, CallbackQuery, InlineKeyboardMarkup, ReplyKeyboardMarkup, \
    InputMediaPhoto, Video
from typing import Tuple, Iterable, Dict, List, DefaultDict, Union, Optional

from errors import InfoException
from models import *

from buttons import *

MailingPost = locals().get('MailingPost')
DEBUG = False
formatter = logging.Formatter(
    '%(asctime)s (%(levelname)s - %(name)s): "%(message)s"'
)

console_output_handler = logging.StreamHandler(sys.stderr)
console_output_handler.setFormatter(formatter)
logger.addHandler(console_output_handler)

logger.setLevel(logging.INFO)
telebot.logger.setLevel(logging.INFO)

_next_step_handlers_callback = []


def _except_errors(callback_data: Optional[str] = 'return_to_start_menu', is_call: Optional[bool] = False):
    def decorator(func):
        @wraps(func)
        def wrapped(_self, message: (Message, CallbackQuery), *args, **kwargs):
            try:
                return func(_self, message, *args, **kwargs)
            except Exception as e:

                if type(e) == ApiException:
                    exception_telegram_dict = json.loads(
                        '{' + (str(e).split(r"[b'{")[-1].split(r"}']")[0]) + '}')
                    if exception_telegram_dict['description'] == 'Bad Request: message is not modified:' \
                                                                 ' specified new message content and reply markup' \
                                                                 ' are exactly the same as a current content and' \
                                                                 ' reply markup of the message':
                        return
                    else:
                        logger.error(f'{type(e)}: ({message.message.chat.id if is_call else message.chat.id}) - {e}')
                        e = exception_telegram_dict['description']
                else:
                    logger.error(f'{type(e)}: ({message.message.chat.id if is_call else message.chat.id}) - {e}')

                if DEBUG:
                    text = "Ошибка:\n" \
                           f"{e}"
                else:
                    text = 'Ошибка'
                try:
                    if is_call:
                        _self.edit_message_text(message.message, text,
                                                reply_markup=return_markup(callback_data=callback_data),
                                                )
                    else:
                        _self.send_message(message.chat.id, text,
                                           reply_markup=return_markup(callback_data=callback_data),
                                           )
                except ApiException as er:
                    logger.error(f'ApiException: ({message.message.chat.id if is_call else message.chat.id}) - {er}')

        return wrapped

    return decorator


def _get_session():
    def decorator(func):
        @wraps(func)
        def wrapper(_self, *args, **kwargs):
            session = _self.Session()
            try:
                try:
                    func(_self, session, *args, **kwargs)
                except Exception as e:
                    session.commit()
                    raise e
                session.commit()
            except SQLAlchemyError as er:
                session.rollback()
                raise er
            finally:
                session.close()

        return wrapper

    return decorator


def _async_get_session(raise_error: Optional[int] = False):
    def decorator(func):
        @wraps(func)
        async def wrapper(_self, *args, **kwargs):
            session = _self.Session()
            try:
                await func(_self, session, *args, **kwargs)
            except Exception as e:
                session.commit()
                session.close()
                if raise_error:
                    raise e
                else:
                    logger.error(f'{func}: {e}')
            session.commit()
            session.close()

        return wrapper

    return decorator


class TeleBot(telebot.TeleBot):
    # Session: scoped_session
    next_step_handlers_callback = {}

    def __init__(self, token: str, session_engine: Engine, *args, **kwargs):
        super().__init__(token, *args, **kwargs)
        self.Session = scoped_session(sessionmaker(bind=session_engine, expire_on_commit=False))
        # super(TeleBot, self).__init__(token, *args, **kwargs)
        logger.info(token)
        self.me = self.get_me()
        global _next_step_handlers_callback
        for callback_name in _next_step_handlers_callback:
            self.next_step_handlers_callback[callback_name] = self.__getattribute__(callback_name)
        if MailingPost:
            self.mailing_post = MailingPost()

        # self.message_handler(func=lambda m: m.chat.type == 'private', commands=['start'])(self._start)
        # self.message_handler(func=lambda m: m.chat.type == 'private', commands=['admins'])(self._create_admin)
        self.message_handler(func=lambda m: m.chat.type == 'private', commands=['reboot'])(self._reboot)
        logger.info('\n######################## Bot init ########################')

    def register_next_step_handlers_callback(self=None, step_again_if_error=True):
        def decorator(func):
            if self:
                self.next_step_handlers_callback[func.__name__] = \
                    self.register_step_again_if_error()(func) if step_again_if_error else func
            else:
                global _next_step_handlers_callback
                _next_step_handlers_callback.append(func.__name__)
            return func

        return decorator

    def get_file_url(self, file_id: str):
        return f"https://api.telegram.org/file/bot{self.token}/{self.get_file(file_id)['file_path']}"

    def edit_message_text(self, message: Message, text: str, inline_message_id: Optional[int] = None,
                          parse_mode: Optional[str] = None,
                          disable_web_page_preview: Optional[bool] = None,
                          reply_markup: Optional[Union[InlineKeyboardMarkup, ReplyKeyboardMarkup]] = None) -> Message:
        return super().edit_message_text(text, message.chat.id, message.message_id, inline_message_id,
                                         parse_mode,
                                         disable_web_page_preview, reply_markup)

    def except_errors(self, callback_data: Optional[str] = 'return_to_start_menu', is_call: Optional[bool] = False):
        def decorator(func):
            @wraps(func)
            def wrapped(message: (Message, CallbackQuery), *args, **kwargs):
                try:
                    return func(message, *args, **kwargs)
                except Exception as e:

                    if str(e).startswith('A request to the Telegram API was unsuccessful'):
                        try:
                            exception_telegram_dict = json.loads(
                                '{' + (str(e).split(r"[b'{")[-1].split(r"}']")[0]) + '}')
                            if exception_telegram_dict['description'] \
                                    == 'Bad Request: message is not modified:' \
                                       ' specified new message content and reply markup' \
                                       ' are exactly the same as a current content and' \
                                       ' reply markup of the message':
                                return
                        except:
                            pass
                    else:
                        logger.error(f'{type(e)}: ({message.message.chat.id if is_call else message.chat.id}) - {e}')
                    if DEBUG or isinstance(e, InfoException):
                        text = "Ошибка:\n" \
                               f"{e}"
                    else:
                        text = 'Ошибка'
                    try:
                        if is_call:
                            message = message.message
                        self.send_message(message.chat.id, text,
                                          reply_markup=return_markup(callback_data=callback_data),
                                          )
                    except ApiException as er:
                        logger.info(f'ApiException: ({message.chat.id}) - {er}')

            return wrapped

        return decorator

    def get_session(self):
        def decorator(func):
            @wraps(func)
            def wrapper(*args, **kwargs):
                session = self.Session()
                try:
                    try:
                        func(session, *args, **kwargs)
                    except Exception as e:
                        session.commit()
                        raise e
                    session.commit()
                except SQLAlchemyError as er:
                    session.rollback()
                    raise er
                finally:
                    session.close()

            return wrapper

        return decorator

    def register_step_again_if_error(self):
        def decorator(func):
            @wraps(func)
            def wrapper(message, *args):
                try:
                    func(message, *args)
                except Exception as e:
                    self.register_next_step_handler(message, func, *args)
                    logger.error(f'{type(e)}: ({message.chat.id}) - {e}')

            return wrapper

        return decorator

    def async_get_session(self, raise_error: int = False):
        def decorator(func):
            @wraps(func)
            async def wrapper(*args, **kwargs):
                session = self.Session()
                try:
                    try:
                        await func(session, *args, **kwargs)
                    except Exception as e:
                        session.commit()
                        raise e
                    session.commit()
                except SQLAlchemyError as er:
                    session.rollback()
                    raise er
                finally:
                    session.close()

            return wrapper

        return decorator

    @_except_errors()
    @_get_session()
    def _create_admin(self, _session: sql_Session, message: Message):
        try:
            user = _session.query(User).filter_by(chat_id=message.chat.id).one()
            if user.is_admin:
                text = 'Меню Адміністратора'
                self.send_message(chat_id=message.chat.id,
                                  text=text,
                                  reply_markup=admin_menu_markup(),
                                  parse_mode="Markdown")
        except:
            self.clear_step_handler(message)

    @_except_errors()
    @_get_session()
    def _start(self, session: sql_Session, message: Message):
        q = session.query(User).filter_by(chat_id=message.chat.id)
        logger.info(f'{q.count()}')
        if not q.count():
            user = User(chat_id=message.chat.id, username=message.chat.username, first_name=message.chat.first_name,
                        last_name=message.chat.last_name)
            session.add(user)
        else:
            user = q.one()
            if user.bot_ban:
                user.bot_ban = False
        text = 'Меню Адміністратора' if user.is_admin else 'Меню'
        self.send_message(chat_id=message.chat.id,
                          text=text,
                          reply_markup=admin_menu_markup() if user.is_admin else menu_markup(),
                          parse_mode="Markdown")

    @_except_errors()
    @_get_session()
    def _reboot(self, session: sql_Session, message: Message):
        q = session.query(User).filter_by(chat_id=message.chat.id)
        if not q.count():
            # raise Exception('Вы не зарегистрованы в этом боте')
            return
        user = q.one()
        if user.is_admin:
            text = 'Бот перезапуститься. Спробуйте звернутися через деяких час.'
            self.send_message(message.chat.id, text)
            os.abort()

    @register_next_step_handlers_callback()
    @_except_errors()
    def _post_message_next_step(self, message: Message):
        # print(message)
        if message.content_type == 'media':
            self.mailing_post.text = message.html_caption
            self.mailing_post.type = 'media'
            self.mailing_post.file_id = message.media
        elif message.photo:
            self.mailing_post.text = message.html_caption
            self.mailing_post.type = 'photo'
            self.mailing_post.file_id = message.photo[-1].file_id
        elif message.video:
            self.mailing_post.text = message.html_caption
            self.mailing_post.type = 'video'
            self.mailing_post.file_id = message.video.file_id
        elif message.document:
            self.mailing_post.text = message.html_caption
            self.mailing_post.type = 'document'
            self.mailing_post.file_id = message.document.file_id
        elif message.text:
            self.mailing_post.text = message.html_text
            self.mailing_post.type = 'text'
        text = 'Выберите действие'
        self.send_message(message.chat.id, text, reply_markup=post_message_configuration_markup())

    @register_next_step_handlers_callback()
    @_except_errors()
    def _post_message_attach_media(self, message: Message):
        if message.media_group_id and message.photo:
            self.mailing_post.type = 'media'
            self.mailing_post.file_id = message.media
        elif message.photo:
            self.mailing_post.type = 'photo'
            self.mailing_post.file_id = message.photo[-1].file_id
        elif message.video:
            self.mailing_post.type = 'video'
            self.mailing_post.file_id = message.video.file_id
        elif message.document:
            self.mailing_post.type = 'document'
            self.mailing_post.file_id = message.document.file_id
        else:
            self.send_message(message.chat.id, 'Ошибка')
            text = 'Выберите действие'
            self.send_message(message.chat.id, text, reply_markup=post_message_configuration_markup())
        text = 'Медиафайл успешо прикреплён. ' \
               'Выберите следующее действие'
        self.send_message(message.chat.id, text, reply_markup=post_message_configuration_markup())

    # @_get_session()
    # def _notify_next_handlers(self, session: sql_Session, new_messages: List[Message]):
    #     i = 0
    #     media = []
    #     media_group_id = None
    #     last_message = None
    #     while i < len(new_messages):
    #         message = new_messages[i]
    #         chat_id = message.chat.id
    #         was_poped = False
    #         if message.media_group_id and message.photo:
    #             if message.media_group_id == media_group_id or media_group_id is None:
    #                 media_group_id = message.media_group_id
    #                 media.append(
    #                     InputMediaPhoto(media=message.photo[-1], caption=message.html_caption, parse_mode='html'))
    #                 last_message = message
    #                 new_messages.pop(i)
    #                 continue
    #             else:
    #                 last_message.__setattr__('media', media)
    #                 last_message.content_type = 'media'
    #                 next_step_handler = session.query(NextStepHandler).get(chat_id)
    #                 if next_step_handler:
    #                     handler_callback = self.next_step_handlers_callback[next_step_handler.callback]
    #                     args = []
    #                     for arg in next_step_handler.args:
    #                         args.append(arg.__getattribute__(arg.type))
    #                     self._exec_task(handler_callback, last_message, *args)
    #                     session.delete(next_step_handler)
    #                 media = []
    #                 media_group_id = message.media_group_id
    #
    #         next_step_handler = session.query(NextStepHandler).get(chat_id)
    #         if next_step_handler:
    #             handler_callback = self.next_step_handlers_callback[next_step_handler.callback]
    #             args = []
    #             for arg in next_step_handler.args:
    #                 args.append(arg.__getattribute__(arg.type))
    #             self._exec_task(handler_callback, message, *args)
    #             session.delete(next_step_handler)
    #             new_messages.pop(i)  # removing message that detects with next_step_handler
    #             was_poped = True
    #         if (not was_poped):
    #             i += 1
    #     if media and last_message:
    #         message = last_message
    #         message.__setattr__('media', media)
    #         message.content_type = 'media'
    #         chat_id = message.chat.id
    #
    #         next_step_handler = session.query(NextStepHandler).get(chat_id)
    #         if next_step_handler:
    #             handler_callback = self.next_step_handlers_callback[next_step_handler.callback]
    #             args = []
    #             for arg in next_step_handler.args:
    #                 args.append(arg.__getattribute__(arg.type))
    #             self._exec_task(handler_callback, message, *args)
    #             session.delete(next_step_handler)
    @_get_session()
    def _notify_next_handlers(self, session: sql_Session, new_messages: List[Message]):
        i = 0
        while i < len(new_messages):
            message = new_messages[i]
            chat_id = message.chat.id
            was_poped = False

            next_step_handler = session.query(NextStepHandler).get(chat_id)
            if next_step_handler:
                handler_callback = self.next_step_handlers_callback[next_step_handler.callback]
                args = []
                for arg in next_step_handler.args:
                    args.append(arg.__getattribute__(arg.type))
                session.delete(next_step_handler)
                session.commit()
                # print(next_step_handler.callback)
                if next_step_handler.callback in ['new_photo_in_slider', 'new_command_handler2', 'add_users_handler']:
                    handler_callback(message, *args)
                else:
                    self._exec_task(handler_callback, message, *args)
                new_messages.pop(i)  # removing message that detects with next_step_handler
                was_poped = True
            if (not was_poped):
                i += 1

    @_get_session()
    def register_next_step_handler_by_chat_id(self, session: sql_Session, chat_id: int, callback, *args, **kwargs):
        next_step_handler = session.query(NextStepHandler).get(chat_id)
        if next_step_handler:
            session.delete(next_step_handler)
        next_step_handler = NextStepHandler(chat_id=chat_id, callback=callback.__name__)
        for arg in args:
            if type(arg) is str:
                _arg = Arg(type='str', str=arg)
            elif type(arg) is int:
                _arg = Arg(type='int', int=arg)
            elif type(arg) is bool:
                _arg = Arg(type='bool', bool=arg)
            elif arg is None:
                _arg = Arg(type='str')
            else:
                raise TypeError('Підтримуються тільки str, bool, int')
            next_step_handler.args.append(_arg)

        session.add(next_step_handler)

    @_get_session()
    def clear_step_handler_by_chat_id(self, session: sql_Session, chat_id: int):
        next_step_handler = session.query(NextStepHandler).get(chat_id)
        if next_step_handler:
            session.delete(next_step_handler)

    def process_new_messages(self, new_messages: List[Message]):
        new_messages_info = ''
        for message in new_messages:
            new_messages_info += f'\n{message}'
        logger.info(f'New messages: {new_messages_info}')
        self._notify_next_handlers(new_messages)
        self._notify_reply_handlers(new_messages)
        self.__notify_update(new_messages)
        self._notify_command_handlers(self.message_handlers, new_messages)

    def callback_query_decorator(self):
        def decorator(func):
            @self.callback_query_handler(func=lambda call: True)
            @self.except_errors(is_call=1)
            @self.get_session()
            def wrapper(session: sql_Session, call: CallbackQuery):
                if call.message.chat.type != 'private':
                    return
                message = call.message
                q = session.query(User).filter_by(chat_id=message.chat.id)
                if not q.count():
                    return
                    # raise Exception('Вы не зарегистрованы в этом боте')
                user = q.one()
                cdata = call.data.split('__')
                logger.info(
                    f'Callback Query: {user.first_name} {user.last_name or ""} ({user.username or user.chat_id}) -> {cdata}'.replace(
                        '  ', ' '))

                # ~start~
                # Обработчик клика на inline-кнопку
                clear_step_handler = func(session, call, message, cdata, user)

                if clear_step_handler is None:
                    self.callback_inline_default(session=session, call=call, cdata=cdata, user=user)

                elif clear_step_handler:
                    self.clear_step_handler(message)

            return wrapper

        return decorator

    def callback_inline_default(self, session: sql_Session, call: CallbackQuery, cdata: list,
                                user: User):
        clear_step_handler = False
        message = call.message

        if cdata[0] == 'return_to_start_menu':
            if user.is_admin:
                text = 'Меню Адміністратора'
                send_markup = admin_menu_markup()
            else:
                text = 'Основне меню'
                send_markup = menu_markup()
            if len(cdata) > 1:
                self.delete_message(message.chat.id, message.message_id)
                self.send_message(message.chat.id, text=text,
                                  reply_markup=send_markup,
                                  parse_mode="Markdown")
            else:
                self.edit_message_text(message=message,
                                       text=text,
                                       reply_markup=send_markup,
                                       parse_mode="Markdown")
        elif cdata[0] == 'statistics':
            count = session.query(User).count()
            count_ban = session.query(User).filter_by(bot_ban=True).count()
            count_admin = session.query(User).filter_by(is_admin=True).count()
            one_day_ago = datetime.utcnow() - timedelta(days=1)

            count_new_users = session.query(User).filter(User.added >= one_day_ago).count()
            text = f'Всього користувачів: {count}\nНових за останні 24 години: {count_new_users}' \
                   f'\nЗаблокували бота: {count_ban}\nАдміністраторів: {count_admin}'
            self.edit_message_text(message,
                                   text=text, reply_markup=return_markup())
        elif cdata[0] == 'get_db':
            if user.is_admin:
                with open('telegram_bot.db', 'rb') as f:
                    self.send_document(user.chat_id, f)
                self.delete_message(chat_id=message.chat.id, message_id=message.message_id)
                text = 'Меню адміністратора'
                self.send_message(
                    chat_id=user.chat_id,
                    text=text, reply_markup=admin_menu_markup())

        # MailingPost
        if cdata[0] == 'mailing':
            print(self.mailing_post)
            if len(cdata) < 2:
                text = f'Надішліть боту повідомлення, яке бажаєте розіслати усім користувачам бота'
                self.mailing_post = MailingPost()
                self.send_message(chat_id=message.chat.id, text=text)
                self.delete_message(chat_id=message.chat.id, message_id=message.message_id)
                self.clear_step_handler(message)
                clear_step_handler = False
                self.register_next_step_handler(call.message, self._post_message_next_step)


            elif cdata[1] == 'publish':
                users = session.query(User).filter(User.bot_ban.isnot(True)).all()
                self.edit_message_text(message,
                                       text='Розсилка почалась',
                                       reply_markup=return_markup('return_to_start_menu'))
                all_count_users = len(users)
                count_ = 0
                count_blocked = 0
                for i, user in enumerate(users):
                    if i % 20 == 0:
                        sleep(1)
                    try:
                        if self.mailing_post.type == 'photo':
                            self.send_photo(user.chat_id, photo=self.mailing_post.file_id,
                                            caption=self.mailing_post.text,
                                            parse_mode=self.mailing_post.parse_mode)
                        elif self.mailing_post.type == 'video':
                            self.send_video(user.chat_id, data=self.mailing_post.file_id,
                                            caption=self.mailing_post.text,
                                            parse_mode=self.mailing_post.parse_mode)
                        elif self.mailing_post.type == 'document':
                            self.send_document(user.chat_id, data=self.mailing_post.file_id,
                                               caption=self.mailing_post.text,
                                               parse_mode=self.mailing_post.parse_mode)
                        elif self.mailing_post.type == 'media':
                            self.send_media_group(chat_id=user.chat_id, media=self.mailing_post.file_id)
                        else:
                            self.send_message(user.chat_id, text=self.mailing_post.text,
                                              parse_mode=self.mailing_post.parse_mode,
                                              disable_web_page_preview=self.mailing_post.disable_web_page_preview)
                        count_ += 1
                        logger.info(f'({count_}/{all_count_users}) Розсилка відправлена користувачу - {user}')
                    except Exception as e:
                        if 'bot was blocked by the user' in str(e):
                            user.bot_ban = True
                            count_blocked += 1
                            logger.info(
                                f'({count_}/{all_count_users}) Розсилка не відправлена користувачу - {user},'
                                f' користувач заблокував бота')
                        logger.info(f'({count_}/{all_count_users}) Розсилка не відправлена користувачу - {user}, '
                                    f'Помилка: {e}')
                self.send_message(chat_id=message.chat.id,
                                  text=f'Відправлено {count_} із {all_count_users}\n'
                                       f'З них заблокували бот - {count_blocked}',
                                  reply_markup=return_markup('return_to_start_menu'))
            elif cdata[1] == 'attach_media':
                text = 'Надішліть боту медіафайл(зображення, відео, GIF)'
                self.send_message(chat_id=message.chat.id, text=text)
                self.delete_message(chat_id=message.chat.id, message_id=message.message_id)
                self.clear_step_handler(message)
                clear_step_handler = False
                self.register_next_step_handler(call.message, self._post_message_attach_media)

            elif cdata[1] == 'clear':
                text = f'Надішліть боту повідомлення, яке бажаєте розіслати усім користувачам бота'
                self.send_message(chat_id=message.chat.id, text=text, parse_mode='HTML')
                self.delete_message(chat_id=message.chat.id, message_id=message.message_id)
                self.clear_step_handler(message)
                clear_step_handler = False
                self.register_next_step_handler(call.message, self._post_message_next_step)

            elif cdata[1] == 'preview':
                self.delete_message(chat_id=message.chat.id, message_id=message.message_id)
                try:
                    if self.mailing_post.type == 'text':
                        self.send_message(chat_id=message.chat.id, text=self.mailing_post.text,
                                          parse_mode=self.mailing_post.parse_mode,
                                          disable_web_page_preview=self.mailing_post.disable_web_page_preview)
                    elif self.mailing_post.type == 'photo':
                        self.send_photo(chat_id=message.chat.id, photo=self.mailing_post.file_id,
                                        caption=self.mailing_post.text,
                                        parse_mode=self.mailing_post.parse_mode)
                    elif self.mailing_post.type == 'video':
                        self.send_video(chat_id=message.chat.id, data=self.mailing_post.file_id,
                                        caption=self.mailing_post.text,
                                        parse_mode=self.mailing_post.parse_mode)
                    elif self.mailing_post.type == 'document':
                        self.send_document(chat_id=message.chat.id, data=self.mailing_post.file_id,
                                           caption=self.mailing_post.text,
                                           parse_mode=self.mailing_post.parse_mode)
                    elif self.mailing_post.type == 'media':
                        self.send_media_group(chat_id=message.chat.id, media=self.mailing_post.file_id)
                    text = 'Оберіть дію'
                    self.send_message(message.chat.id, text,
                                      reply_markup=post_message_configuration_markup())
                except Exception as e:
                    raise e
        if clear_step_handler:
            self.clear_step_handler(message)

    def send_copy_message(self, message: Message, chat_id: int, disable_web_page_preview: Optional[bool] = None,
                          disable_notification: Optional[bool] = None,
                          reply_markup: Optional[Union[InlineKeyboardMarkup, ReplyKeyboardMarkup]] = None):
        if message.content_type == 'text':
            self.send_message(chat_id=chat_id, text=message.html_text,
                              parse_mode='html',
                              disable_web_page_preview=disable_web_page_preview,
                              disable_notification=disable_notification,
                              reply_markup=reply_markup)
        elif message.content_type == 'photo':
            self.send_photo(chat_id=chat_id, photo=message.photo[-1].file_id,
                            caption=message.html_caption,
                            parse_mode='html',
                            disable_notification=disable_notification,
                            reply_markup=reply_markup)
        elif message.content_type == 'video':
            self.send_video(chat_id=chat_id, data=message.video.file_id,
                            caption=message.html_caption,
                            parse_mode='html',
                            disable_notification=disable_notification,
                            reply_markup=reply_markup)
        elif message.content_type == 'document':
            self.send_document(chat_id=chat_id, data=message.document.file_id,
                               caption=message.html_caption,
                               parse_mode='html',
                               disable_notification=disable_notification,
                               reply_markup=reply_markup)
        elif message.content_type == 'voice':
            self.send_voice(chat_id=chat_id, voice=message.voice.file_id,
                            caption=message.html_caption,
                            parse_mode='html',
                            disable_notification=disable_notification,
                            reply_markup=reply_markup)
        elif message.content_type == 'video_note':
            self.send_video_note(chat_id=chat_id, data=message.voice.file_id,
                                 disable_notification=disable_notification,
                                 reply_markup=reply_markup)
        elif message.content_type == 'media':
            self.send_media_group(chat_id=chat_id, media=message.media)
        else:
            raise Exception('this type of message is not supported')

    def send_large_text(self, chat_id, text, disable_web_page_preview=None, reply_to_message_id=None, reply_markup=None,
                        parse_mode=None, disable_notification=None):
        splitted_text = util.split_string(text, 4096)
        for text in splitted_text:
            self.send_message(chat_id=chat_id, text=text, disable_web_page_preview=disable_web_page_preview,
                              reply_to_message_id=reply_to_message_id, reply_markup=reply_markup,
                              parse_mode=parse_mode, disable_notification=disable_notification)
