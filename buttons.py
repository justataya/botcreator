from telebot.types import ReplyKeyboardMarkup

from typing import List
from models import Command, MessageButton, User
from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton


def convert_cdata(*args: str) -> str:
    return '__'.join(args)


def em(emoji: str, text: str) -> str:
    """ Adds emoji in the correct format"""
    return f'{emoji} {text} ᅠ'


left_button = InlineKeyboardButton(text=em('⬅️', 'Назад'),
                                   callback_data='return_to_start_menu')

# menu_markup
_menu_markup = InlineKeyboardMarkup(row_width=1)


def menu_markup() -> InlineKeyboardMarkup:
    return _menu_markup


# admin_menu_markup
_admin_menu_markup = InlineKeyboardMarkup(row_width=1)
# _admin_menu_markup.keyboard += _menu_markup.keyboard

_button = InlineKeyboardButton(text=em('🔊', 'Створити розсилку'),
                               callback_data='mailing')
_admin_menu_markup.add(_button)
_admin_menu_markup.add(

    InlineKeyboardButton(text=em('⚙️', 'Користувацькі команди'),
                         callback_data='change_commands'),
    InlineKeyboardButton(text=em('👤', 'Користувачі'),
                         callback_data='users'),
    InlineKeyboardButton(text=em('👥', 'Налаштувати групи'),
                         callback_data='groups'),
    InlineKeyboardButton(text=em('📊', 'Статистика'),
                         callback_data='statistics')
)


def admin_menu_markup() -> InlineKeyboardMarkup:
    return _admin_menu_markup


def return_markup(callback_data: str = 'return_to_start_menu', text: str = em('⬅️', 'Назад')) -> InlineKeyboardMarkup:
    markup = InlineKeyboardMarkup()
    button = InlineKeyboardButton(text=text,
                                  callback_data=callback_data)
    markup.add(button)
    return markup


def return_markup_2(callback_data: str) -> InlineKeyboardMarkup:
    markup = InlineKeyboardMarkup()
    button = InlineKeyboardButton(text=em('🔚', 'Головна'),
                                  callback_data='return_to_start_menu')
    markup.add(button)
    button = InlineKeyboardButton(text=em('⬅️', 'Назад'),
                                  callback_data=callback_data)
    markup.add(button)
    return markup


def cancel_markup(callback_data: str = 'return_to_start_menu') -> InlineKeyboardMarkup:
    return return_markup(text=em('⬅️', 'Відміна'), callback_data=callback_data)


def post_message_configuration_markup() -> InlineKeyboardMarkup:
    markup = InlineKeyboardMarkup(row_width=2)
    markup.add(
        InlineKeyboardButton(text=em('🔊', 'Опубліковати'),
                             callback_data=convert_cdata('mailing', 'publish')),
        InlineKeyboardButton(text=em('🔗', 'Прикріпити медіафайл'),
                             callback_data=convert_cdata('mailing', 'attach_media'))
    )
    markup.add(
        InlineKeyboardButton(text=em('🔄', 'Очистити'),
                             callback_data=convert_cdata('mailing', 'clear')),
        InlineKeyboardButton(text=em('🧐', 'Перегляд'),
                             callback_data=convert_cdata('mailing', 'preview'))
    )
    markup.add(
        InlineKeyboardButton(text=em('⬅️', 'Відміна'),
                             callback_data='return_to_start_menu')
    )
    return markup


def generate_markup(user: User, buttons: List[MessageButton], one_time_keyboard: bool) -> ReplyKeyboardMarkup:
    markup = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=one_time_keyboard)
    for button in buttons:
        if user.is_admin:
            markup.row(button.text)
        elif button.command.groups_type == 'all':
            markup.row(button.text)
        elif button.command.groups_type == 'all_reg':
            if user.group:
                markup.row(button.text)
        elif user.group in button.command.groups:
            markup.row(button.text)
    markup.row('Назад')
    return markup


def generate_slider_markup(num_page: int, message_id: int, len_messages: int) -> InlineKeyboardMarkup:
    markup = InlineKeyboardMarkup()
    markup.add(
        InlineKeyboardButton(text='⬅️',
                             callback_data=convert_cdata('slider', str(message_id),
                                                         str(len_messages - 1 if num_page == 0 else num_page - 1))
                             ),
        InlineKeyboardButton(text=f'{num_page + 1}/{len_messages}', callback_data='pass'),
        InlineKeyboardButton(text='️➡',
                             callback_data=convert_cdata('slider', str(message_id),
                                                         str(0 if num_page + 1 == len_messages else num_page + 1))
                             ),

    )
    return markup


def change_menu_markup(commands: List[Command]) -> ReplyKeyboardMarkup:
    markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)

    markup.row('Створити команду', 'Налаштувати головне меню')
    for command in commands:
        markup.row(f'/{command.name}')
    markup.row('Назад')
    return markup


def reply_1_markup(text: str) -> ReplyKeyboardMarkup:
    markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    markup.row(text)
    return markup


def cancel_reply_markup() -> ReplyKeyboardMarkup:
    markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    markup.row('Відміна')
    return markup


def new_command_menu_markup() -> ReplyKeyboardMarkup:
    markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    markup.row('Додати слайдер')
    markup.row('Зберегти')
    markup.row('Відміна')
    return markup


def save_or_cancel_markup() -> ReplyKeyboardMarkup:
    markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    markup.row('Зберегти')
    markup.row('Відміна')
    return markup


def new_slider_markup() -> ReplyKeyboardMarkup:
    return save_or_cancel_markup()


def change_gl_menu_markup(message_buttons: List[MessageButton]) -> ReplyKeyboardMarkup:
    markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    for button in message_buttons:
        markup.row(button.text)
    markup.row('➕ Додати пункт меню ➕')
    markup.row('Назад')
    return markup


def choice_command_markup(commands: List[Command]) -> ReplyKeyboardMarkup:
    markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    for command in commands:
        markup.row(f'/{command.name}')
    markup.row('Назад')
    return markup


def button_menu_markup(button_text: str, one_time_keyboard: bool) -> ReplyKeyboardMarkup:
    markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    markup.row(button_text)
    markup.row(f'Режим меню: {"Зникаюче" if one_time_keyboard else "Закріплене"}')
    markup.row('Переіменувати пункт меню')
    markup.row('Прибрати цей пункт з меню')
    markup.row('Відміна')
    return markup


def change_command_menu_markup() -> ReplyKeyboardMarkup:
    markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    markup.row('Показати команду')
    markup.row('Редагувати відповіді команди')
    markup.row('Налаштувати доступ до команди')
    markup.row('Налаштувати меню команди')
    markup.row('Видалити команду')
    markup.row('Назад')
    return markup


def delete_message_bot_yes_or_not_markup() -> ReplyKeyboardMarkup:
    markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    markup.row('Видалити повідомлення')
    markup.row('Відміна')
    return markup


def delete_all_messages_yes_or_not_markup() -> ReplyKeyboardMarkup:
    markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    markup.row('Видалити всі повідомлення')
    markup.row('Відміна')
    return markup


def delete_command_yes_or_not_markup() -> ReplyKeyboardMarkup:
    markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    markup.row('Видалити команду')
    markup.row('Відміна')
    return markup


def next_change_command_markup() -> ReplyKeyboardMarkup:
    markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    markup.row('Додати повідомлення до команди')
    markup.row('Видалити всі повідомлення')
    markup.row('Назад')
    return markup


def next_change_command_messages_markup() -> ReplyKeyboardMarkup:
    markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    markup.row('Додати слайдер')
    markup.row('Зберігти')
    return markup


def users_list_markup(users: List[User]) -> InlineKeyboardMarkup:
    markup = InlineKeyboardMarkup()
    us2 = []
    for user in users:
        us2.append(
            InlineKeyboardButton(
                text=f'{user.first_name} {user.last_name or ""}'.strip(),
                url=f't.me/{user.username}' if user.username else None,
                callback_data='udhu' if not user.username else None
            )
        )
        if len(us2) == 2:
            markup.add(*us2)
            us2.clear()
    if us2:
        markup.add(*us2)
    return markup


def users_markup() -> ReplyKeyboardMarkup:
    markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    markup.row('Додати користувачів до групи')
    markup.row('Видалити користувачів')
    markup.row('Назад')
    return markup


def groups_markup() -> ReplyKeyboardMarkup:
    markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    markup.row('Додати групи')
    markup.row('Видалити групи')
    markup.row('Назад')
    return markup


def choice_groups_markup(groups_name: list) -> ReplyKeyboardMarkup:
    markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    gr2 = []
    for group_name in groups_name:
        gr2.append(group_name)
        if len(gr2) == 2:
            markup.row(*gr2)
            gr2.clear()
    if gr2:
        markup.row(*gr2)
    markup.row('Відміна')
    return markup
