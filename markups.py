from telebot import types

init_markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
new_bot_button = types.KeyboardButton('Додати нового бота')  # /add_bot
get_bots_button = types.KeyboardButton('Показати існуючих ботів')  # /get_bots
stop_bot_button = types.KeyboardButton('Зупинити бота')  # /stop_bot
activate_bot_button = types.KeyboardButton('Активувати бота')  # /activate_bot
delete_bot_button = types.KeyboardButton('Видалити бота')  # /delete_bot
help_button = types.KeyboardButton('Допомогти')  # /help
init_markup.add(new_bot_button, get_bots_button, stop_bot_button, activate_bot_button, delete_bot_button, help_button)

back_button = types.KeyboardButton('Назад')


def hello_message(name):
    return f'''Добрий день, {name}!

Ми допоможемо вам створити свого бота.
Щоб додати свого першого бота, використайте команду /add_bot.'''


add_bot_message = '''1⃣ Перейдіть до @BotFather. Для цього натисніть на його ім'я, а потім 'Send Message', якщо це буде необхідно.
2⃣ Створіть нового бота у нього. Для цього всередині @BotFather використайте команду 'newbot' (спочатку вам потрібно буде придумати назву, воно може бути будь-якою мовою; потім потрібно придумати ваше посилання, воно повонне бути англійською та обов'язково закінчуватись на 'bot', наприклад, 'NewsBot').
3⃣ Скопіюйте API токен, котрий вам видасть @BotFather
4⃣ Повертайтесь назад и надішліть скопійований API токен у відповідь на це повідомлення.'''

menu_message = '''Оберіть команду:'''

show_bots_message = '''Ваші боти:'''

stop_bot_message = '''Оберіть бота, якого хочете зупинити.'''

activate_bot_message = '''Введіть токен бота, якого хочете активувати.'''

delete_bot_message = '''Оберіть бота, якого хочете видалити.'''

help_message = '''Наявні команди:
/add_bot - додає нового бота
/get_bots - показує усіх створених вами ботів
/stop_bot - зупиняє одного з ваших активних ботів
/activate_bot - активує призупиненого бота
/delete_bot - повністю видаляє дані створеного вами бота'''

no_bots_message = '''У вас ще немає ботів. Використайте команду /add_bot, щоб створити нового бота.'''

no_active_bots_message = '''У вас ще немає активних ботів. Використайте команду /add_bot, щоб створити нового бота, або активуйте існуючого неактивного бота командою /activate_bot .'''

no_disabled_bots_message = '''У вас немає неактивних ботів.'''
