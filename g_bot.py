import os
from flask import Flask, request
import telebot
import markups
import sub_bot
import datetime
import models

# init bot
bot_token = os.environ.get('TOKEN')
bot = telebot.TeleBot(bot_token, threaded=False)

server = Flask(__name__)

state = None
running_bots = {}


# message handlers
@bot.message_handler(commands=['start'])
def start(message):
    user = models.session.query(models.User).filter_by(chat_id=message.chat.id).one_or_none()
    if not user:
        new_user = models.User(chat_id=message.chat.id,
                               username=message.from_user.username,
                               first_name=message.from_user.first_name,
                               last_name=message.from_user.last_name,
                               added=datetime.datetime.now())
        models.session.add(new_user)
        models.session.commit()
    bot.send_message(message.chat.id, markups.hello_message(message.from_user.first_name),
                     reply_markup=markups.init_markup)
    init_sub_bots()


def init_sub_bots():
    global running_bots
    bots = models.session.query(models.Bot).filter_by(is_active=True).all()
    for _bot in bots:
        if _bot.token not in running_bots.keys():
            new_sub_bot = sub_bot.Bot(_bot.token)
            running_bots[_bot.token] = new_sub_bot


@bot.message_handler(func=lambda message: message.text in ('Додати нового бота', '/add_bot'))
def add_bot(message):
    global state
    state = "wait_token"
    bot.send_message(message.chat.id, markups.add_bot_message)


@bot.message_handler(func=lambda message: message.text in ('Показати існуючих ботів', '/get_bots'))
def get_bots(message):
    user = models.session.query(models.User).filter_by(chat_id=message.chat.id).one_or_none()
    if user is not None:
        bots = models.session.query(models.UserBotAssociation).filter_by(user_id=user.id).all()
        reply = markups.show_bots_message
        for _bot in bots:
            if _bot.bot.token in running_bots.keys():
                reply = reply + f"\n@{running_bots[_bot.bot.token].bot.get_me().username}"
        bot.send_message(message.chat.id, reply)


@bot.message_handler(func=lambda message: message.text in ('Зупинити бота', '/stop_bot'))
def stop_bot(message):
    user = models.session.query(models.User).filter_by(chat_id=message.chat.id).one_or_none()
    if user is not None:
        bots = models.session.query(models.UserBotAssociation).filter_by(user_id=user.id, is_active=True).all()
        global state
        state = "stop_bot"
        if len(bots) < 1:
            bot.send_message(message.chat.id, markups.no_active_bots_message)
        else:
            bot.send_message(message.chat.id, markups.stop_bot_message, reply_markup=show_bots_markup(bots))


@bot.message_handler(func=lambda message: message.text in ('Активувати бота', '/activate_bot'))
def activate_bot(message):
    user = models.session.query(models.User).filter_by(chat_id=message.chat.id).one_or_none()
    if user is not None:
        bots = models.session.query(models.UserBotAssociation).filter_by(user_id=user.id, is_active=False).all()
        global state
        state = "activate_bot"
        if len(bots) < 1:
            bot.send_message(message.chat.id, markups.no_disabled_bots_message)
        else:
            bot.send_message(message.chat.id, markups.activate_bot_message, reply_markup=show_bots_markup(bots))


@bot.message_handler(func=lambda message: message.text in ('Видалити бота', '/delete_bot'))
def delete_bot(message):
    user = models.session.query(models.User).filter_by(chat_id=message.chat.id).one_or_none()
    if user is not None:
        bots = models.session.query(models.UserBotAssociation).filter_by(user_id=user.id).all()
        global state
        state = "delete_bot"
        if len(bots) < 1:
            bot.send_message(message.chat.id, markups.no_bots_message)
        else:
            bot.send_message(message.chat.id, markups.stop_bot_message, reply_markup=show_bots_markup(bots))


def show_bots_markup(bots):
    bots_markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
    for _bot in bots:
        if _bot.bot.token in running_bots.keys():
            bot_button = telebot.types.KeyboardButton(running_bots[_bot.bot.token].bot.get_me().username)
            bots_markup.add(bot_button)
    return bots_markup


@bot.message_handler(func=lambda message: message.text in ('Назад', '/back'))
def go_back(message):
    global state
    state = None
    bot.send_message(message.chat.id, markups.menu_message, reply_markup=markups.init_markup)


@bot.message_handler(func=lambda message: message.text in ('Допомогти', '/help'))
def go_back(message):
    global state
    state = None
    bot.send_message(message.chat.id, markups.menu_message, reply_markup=markups.init_markup)


@bot.message_handler(content_types=["text"])
def common_message(message):
    global state, server
    if state == "wait_token":
        state = None
        create_bot(message)
    elif state == "stop_bot":
        state = None
        stop_bot_by_name(message)
    elif state == "activate_bot":
        state = None
        activate_bot_by_token(message)
    elif state == "delete_bot":
        state = None
        delete_bot_by_name(message)


def create_bot(message):
    sub_bot_token = message.text
    sub_bot_by_token = models.session.query(models.Bot).filter_by(token=sub_bot_token).one_or_none()
    if sub_bot_by_token is not None:
        bot.send_message(message.chat.id, 'Bot is already exists')
    else:
        new_sub_bot = models.Bot(token=sub_bot_token)
        models.session.add(new_sub_bot)
        new_sub_bot = models.session.query(models.Bot).filter_by(token=sub_bot_token).one()
        current_user = models.session.query(models.User).filter_by(chat_id=message.chat.id).one()
        sub_bot_to_user = models.UserBotAssociation(bots_id=new_sub_bot.id,
                                                    user_id=current_user.id,
                                                    is_admin=True,
                                                    is_user_mode=False,
                                                    user=current_user,
                                                    bot=new_sub_bot)
        models.session.add(sub_bot_to_user)
        models.session.commit()

        new_sub_bot = sub_bot.Bot(message.text)
        running_bots[message.text] = new_sub_bot
        bot.send_message(message.chat.id, f'Бот @{new_sub_bot.bot.get_me().username} запущено',
                         reply_markup=markups.init_markup)


def stop_bot_by_name(message):
    remove_token = None
    for _token, _bot in running_bots.items():
        if _bot.bot.get_me().username == message.text:
            _bot.bot.stop_bot()
            _bot_model = models.session.query(models.Bot).filter_by(token=_token).one()
            _bot_model.is_active = False
            user_bots = models.session.query(models.UserBotAssociation).filter_by(bots_id=_bot_model.id).all()
            for _user_bot in user_bots:
                _user_bot.is_active = False
            models.session.commit()
            bot.send_message(message.chat.id, f'Бот @{_bot.bot.get_me().username} зупинено',
                             reply_markup=markups.init_markup)
            remove_token = _token
        if remove_token:
            running_bots.pop(remove_token)


def activate_bot_by_token(message):
    new_sub_bot = models.session.query(models.Bot).filter_by(token=message.text).one_or_none()
    if not new_sub_bot:
        bot.send_message(message.chat.id, 'Даного бота не існує',
                         reply_markup=markups.init_markup)
    elif new_sub_bot.is_active:
        bot.send_message(message.chat.id, 'Даний бот уже активний',
                         reply_markup=markups.init_markup)
    new_sub_bot.is_active = True
    user_sub_bot = models.session.query(models.UserBotAssociation).filter_by(bots_id=new_sub_bot.id).one_or_none()
    user_sub_bot.is_active = True
    models.session.commit()
    new_sub_bot = sub_bot.Bot(message.text)
    running_bots[message.text] = new_sub_bot
    bot.send_message(message.chat.id, f'Бот @{new_sub_bot.bot.get_me().username} запущено',
                     reply_markup=markups.init_markup)


def delete_bot_by_name(message):
    for _token, _bot in running_bots.items():
        if _bot.bot.get_me().username == message.text:
            _sub_bot = models.session.query(models.Bot).filter_by(token=_token).one_or_none()
            _bot.bot.stop_bot()
            models.session.delete(_sub_bot)
            models.session.commit()
            bot.send_message(message.chat.id, f'Бот @{_bot.bot.get_me().username} видалено',
                             reply_markup=markups.init_markup)


# webhook and flask server handlers
@server.route('/<token>', methods=['POST'])
def get_message(token):
    if token == bot_token:
        bot.process_new_updates([telebot.types.Update.de_json(request.stream.read().decode("utf-8"))])
    if token in running_bots.keys():
        _bot = running_bots[token]
        _bot.bot.process_new_updates([telebot.types.Update.de_json(request.stream.read().decode("utf-8"))])
    return "!", 200


@server.route('/')
def webhook():
    bot.delete_webhook()
    bot.set_webhook(url='https://bot-creator-app.herokuapp.com/' + bot_token)
    return "!", 200


if __name__ == "__main__":
    server.run(host="0.0.0.0", port=int(os.environ.get('PORT', 5000)), threaded=True)
