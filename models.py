from datetime import datetime

from sqlalchemy import Column, Integer, String, Boolean
from sqlalchemy import create_engine, ForeignKey, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy import Table

Model = declarative_base()
engine = create_engine(
    'postgres://jiimlqjsgpnlqe:e0ef4b660a6c3bcc01bb49bf85991fb198b4c8ad13fa0b0047ccdbcc997f6267@ec2-52-213-119-221.eu-west-1.compute.amazonaws.com:5432/d5sj0imac7urih')

# create a configured "Session" class
Session = sessionmaker(bind=engine)

# create a Session
session = Session()


class UserBotAssociation(Model):
    __tablename__ = 'users_bots'

    user_id = Column(Integer, ForeignKey('users.id', ondelete='CASCADE'), primary_key=True)
    bots_id = Column(Integer, ForeignKey('bots.id', ondelete='CASCADE'), primary_key=True)
    is_admin = Column(Boolean, default=False)
    is_user_mode = Column(Boolean, default=True)
    is_active = Column(Boolean, default=True)
    user = relationship("User", back_populates="bots", cascade="all,delete")
    bot = relationship("Bot", back_populates="users", cascade="all,delete")


class User(Model):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    chat_id = Column(Integer, nullable=False, unique=True)
    username = Column(String)
    first_name = Column(String)
    last_name = Column(String)
    added = Column(DateTime, default=datetime.utcnow)
    bot_ban = Column(Boolean, default=False)
    bots = relationship("UserBotAssociation", back_populates="user", cascade="all,delete")
    group_name = Column(String, ForeignKey('groups.name', ondelete='CASCADE'))
    group = relationship('Group', back_populates='users', cascade="all,delete")


class Bot(Model):
    __tablename__ = 'bots'

    id = Column(Integer, primary_key=True)
    token = Column(String, unique=True)
    is_active = Column(Boolean, default=True)
    users = relationship("UserBotAssociation", back_populates="bot", cascade="all,delete")


def is_admin(bot: Bot, user: User):
    user_bot = session.query(UserBotAssociation).filter_by(bots_id=bot.id, user_id=user.id).one_or_none()
    return user_bot.is_admin


groups_commands = Table('groups_commands', Model.metadata,
                        Column('group_name', ForeignKey('groups.name', ondelete='CASCADE'), primary_key=True),
                        Column('command_name', ForeignKey('commands.name', ondelete='CASCADE'), primary_key=True)
                        )


class MailingPost:
    text = None
    parse_mode = 'HTML'
    type = None
    file_id = None
    disable_web_page_preview = True

    def __repr__(self):
        return f'<MailingPost(text={self.text}, ' \
               f'parse_mode={self.parse_mode}, ' \
               f'type={self.type}, ' \
               f'file_id={self.file_id}, ' \
               f'disable_web_page_preview={self.disable_web_page_preview})>'


class SliderMessage(Model):
    __tablename__ = 'slider_messages'
    id = Column(Integer, primary_key=True)
    text = Column(String(2048))
    file_id = Column(String)

    message_id = Column(Integer, ForeignKey('messages.id', ondelete='CASCADE'))
    message = relationship('MessageBot', back_populates='slider_messages', cascade="all,delete")


class MessageBot(Model):
    __tablename__ = 'messages'
    id = Column(Integer, primary_key=True)

    content_type = Column(String(20))  # text, photo, video, document, voice, video_note, slider, question
    text = Column(String(4096))
    file_id = Column(String)

    slider_messages = relationship('SliderMessage', back_populates='message',
                                   cascade="all, delete-orphan")  # if messages

    command_name = Column(String, ForeignKey('commands.name', ondelete='CASCADE'))
    command = relationship('Command', back_populates='messages', cascade="all,delete")


class MessageButton(Model):
    __tablename__ = 'buttons'

    id = Column(Integer, primary_key=True)

    text = Column(String, nullable=False, unique=True)

    command_name = Column(String, ForeignKey('commands.name', ondelete='CASCADE'))
    command = relationship('Command', foreign_keys=[command_name], back_populates='button', cascade="all,delete")

    _command_name = Column(String, ForeignKey('commands.name', ondelete='CASCADE'))
    _command = relationship('Command', foreign_keys=[_command_name], back_populates='buttons', cascade="all,delete")


class Group(Model):
    __tablename__ = 'groups'

    name = Column(String, primary_key=True)
    users = relationship('User', back_populates='group', cascade="all,delete")
    commands = relationship('Command',
                            secondary=groups_commands,
                            back_populates='groups', cascade="all,delete")


class Command(Model):
    __tablename__ = 'commands'

    name = Column(String, primary_key=True)
    one_time_keyboard = Column(Boolean, default=False)
    active = Column(Boolean, default=False)
    free = Column(Boolean, default=True)
    groups_type = Column(String(20), default='all')  # all, all_reg, added
    messages = relationship('MessageBot', back_populates='command',
                            cascade="all, delete-orphan")
    button = relationship("MessageButton", uselist=False, back_populates="command",
                          cascade="all, delete-orphan",
                          primaryjoin="MessageButton.command_name==Command.name")
    buttons = relationship('MessageButton', back_populates='_command',
                           cascade="all, delete-orphan",
                           primaryjoin="MessageButton._command_name==Command.name")
    groups = relationship('Group',
                          secondary=groups_commands,
                          back_populates='commands')


class Arg(Model):
    __tablename__ = 'args'

    id = Column(Integer, primary_key=True)
    type = Column(String(20))
    bool = Column(Boolean)
    int = Column(Integer)
    str = Column(String)
    next_step_handler_id = Column(Integer, ForeignKey('next_step_handlers.chat_id', ondelete='CASCADE'))
    next_step_handler = relationship('NextStepHandler', back_populates='args', cascade="all,delete")


class NextStepHandler(Model):
    __tablename__ = 'next_step_handlers'

    chat_id = Column(Integer, primary_key=True)
    callback = Column(String)

    args = relationship('Arg', back_populates='next_step_handler', cascade="all, delete-orphan")


Model.metadata.create_all(engine)
