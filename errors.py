class InfoException(Exception):
    """Occurs when a user makes a mistake"""

    def __init__(self, message: str):
        super().__init__(message)
