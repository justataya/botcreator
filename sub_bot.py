import re

from telebot.types import ReplyKeyboardRemove

import logging
import datetime

from telebot import util

import json
import os
from functools import wraps
from time import sleep

import telebot
from sqlalchemy.engine import Engine
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import scoped_session
# from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm.session import Session as sql_Session
from telebot.apihelper import ApiException
from telebot.types import Message, CallbackQuery, \
    InputMediaPhoto
from typing import Union, Optional

from errors import InfoException
import models

from buttons import *

import utils

# from repository import *

_next_step_handlers_callback = []

DEBUG = False
logger = logging.getLogger('SQL_Bot')


def _get_session():
    def decorator(function):
        @wraps(function)
        def wrapper(_self, *args, **kwargs):
            session = models.Session()
            try:
                try:
                    function(_self, session, *args, **kwargs)
                except Exception as e:
                    session.commit()
                    raise e
                session.commit()
            except SQLAlchemyError as er:
                session.rollback()
                raise er
            finally:
                session.close()

        return wrapper

    return decorator


def _except_errors(callback_data: Optional[str] = 'return_to_start_menu', is_call: Optional[bool] = False):
    def decorator(func):
        @wraps(func)
        def wrapped(_self, message: (Message, CallbackQuery), *args, **kwargs):
            try:
                return func(_self, message, *args, **kwargs)
            except Exception as e:

                if type(e) == ApiException:
                    exception_telegram_dict = json.loads(
                        '{' + (str(e).split(r"[b'{")[-1].split(r"}']")[0]) + '}')
                    if exception_telegram_dict['description'] == 'Bad Request: message is not modified:' \
                                                                 ' specified new message content and reply markup' \
                                                                 ' are exactly the same as a current content and' \
                                                                 ' reply markup of the message':
                        return
                    else:
                        logger.error(f'{type(e)}: ({message.message.chat.id if is_call else message.chat.id}) - {e}')
                        e = exception_telegram_dict['description']
                else:
                    logger.error(f'{type(e)}: ({message.message.chat.id if is_call else message.chat.id}) - {e}')

                if DEBUG:
                    text = "Ошибка:\n" \
                           f"{e}"
                else:
                    text = 'Ошибка'
                try:
                    if is_call:
                        _self.edit_message_text(message.message, text,
                                                reply_markup=return_markup(callback_data=callback_data),
                                                )
                    else:
                        _self.send_message(message.chat.id, text,
                                           reply_markup=return_markup(callback_data=callback_data),
                                           )
                except ApiException as er:
                    logger.error(f'ApiException: ({message.message.chat.id if is_call else message.chat.id}) - {er}')

        return wrapped

    return decorator


class TeleBot(telebot.TeleBot):
    # Session: scoped_session
    next_step_handlers_callback = {}

    def __init__(self, token: str, session_engine: Engine, *args, **kwargs):
        self.Session = scoped_session(models.sessionmaker(bind=session_engine, expire_on_commit=False))
        super(TeleBot, self).__init__(token, *args, **kwargs)
        self.me = self.get_me()
        global _next_step_handlers_callback
        for callback_name in _next_step_handlers_callback:
            self.next_step_handlers_callback[callback_name] = self.__getattribute__(callback_name)
        if models.MailingPost:
            self.mailing_post = models.MailingPost()

        # self.message_handler(func=lambda m: m.chat.type == 'private', commands=['start'])(self._start)
        # self.message_handler(func=lambda m: m.chat.type == 'private', commands=['admins'])(self._create_admin)
        # self.message_handler(func=lambda m: m.chat.type == 'private', commands=['reboot'])(self._reboot)
        logger.info('\n######################## Bot init ########################')

    def get_session(self):
        def decorator(function):
            @wraps(function)
            def wrapper(*args, **kwargs):
                session = self.Session()
                try:
                    try:
                        function(session, *args, **kwargs)
                    except Exception as e:
                        session.commit()
                        raise e
                    session.commit()
                except SQLAlchemyError as er:
                    session.rollback()
                    raise er
                finally:
                    session.close()

            return wrapper

        return decorator

    def register_next_step_handlers_callback(self=None, step_again_if_error=True):
        def decorator(func):
            if self:
                self.next_step_handlers_callback[func.__name__] = \
                    self.register_step_again_if_error()(func) if step_again_if_error else func
            else:
                global _next_step_handlers_callback
                _next_step_handlers_callback.append(func.__name__)
            return func

        return decorator

    def get_file_url(self, file_id: str):
        return f"https://api.telegram.org/file/bot{self.token}/{self.get_file(file_id)['file_path']}"

    def edit_message_text(self, message: Message, text: str, inline_message_id: Optional[int] = None,
                          parse_mode: Optional[str] = None,
                          disable_web_page_preview: Optional[bool] = None,
                          reply_markup: Optional[Union[InlineKeyboardMarkup, ReplyKeyboardMarkup]] = None) -> Message:
        return super().edit_message_text(text, message.chat.id, message.message_id, inline_message_id,
                                         parse_mode,
                                         disable_web_page_preview, reply_markup)

    def except_errors(self, callback_data: Optional[str] = 'return_to_start_menu', is_call: Optional[bool] = False):
        def decorator(func):
            @wraps(func)
            def wrapped(message: (Message, CallbackQuery), *args, **kwargs):
                try:
                    return func(message, *args, **kwargs)
                except Exception as e:

                    if str(e).startswith('A request to the Telegram API was unsuccessful'):
                        try:
                            exception_telegram_dict = json.loads(
                                '{' + (str(e).split(r"[b'{")[-1].split(r"}']")[0]) + '}')
                            if exception_telegram_dict['description'] \
                                    == 'Bad Request: message is not modified:' \
                                       ' specified new message content and reply markup' \
                                       ' are exactly the same as a current content and' \
                                       ' reply markup of the message':
                                return
                        except:
                            pass
                    else:
                        logger.error(f'{type(e)}: ({message.message.chat.id if is_call else message.chat.id}) - {e}')
                    if DEBUG or isinstance(e, InfoException):
                        text = "Ошибка:\n" \
                               f"{e}"
                    else:
                        text = 'Ошибка'
                    try:
                        if is_call:
                            message = message.message
                        self.send_message(message.chat.id, text,
                                          reply_markup=return_markup(callback_data=callback_data),
                                          )
                    except ApiException as er:
                        logger.info(f'ApiException: ({message.chat.id}) - {er}')

            return wrapped

        return decorator

    def register_step_again_if_error(self):
        def decorator(func):
            @wraps(func)
            def wrapper(message, *args):
                try:
                    func(message, *args)
                except Exception as e:
                    self.register_next_step_handler(message, func, *args)
                    logger.error(f'{type(e)}: ({message.chat.id}) - {e}')

            return wrapper

        return decorator

    def async_get_session(self):
        def decorator(func):
            @wraps(func)
            async def wrapper(*args, **kwargs):
                session = self.Session()
                try:
                    try:
                        await func(session, *args, **kwargs)
                    except Exception as e:
                        session.commit()
                        raise e
                    session.commit()
                except SQLAlchemyError as er:
                    session.rollback()
                    raise er
                finally:
                    session.close()

            return wrapper

        return decorator

    @_except_errors()
    @_get_session()
    def _create_admin(self, session: sql_Session, message: Message):
        try:
            bot = session.query(models.Bot).filter_by(token=self.bot.token).one()
            q = session.query(models.User).filter_by(chat_id=message.chat.id)
            if q.count():
                user = q.one()
                if models.is_admin(bot, user):
                    text = 'Меню Адміністратора'
                    self.send_message(chat_id=message.chat.id,
                                      text=text,
                                      reply_markup=admin_menu_markup(),
                                      parse_mode="Markdown")
        except:
            self.clear_step_handler(message)

    @_except_errors()
    @_get_session()
    def _start(self, session: sql_Session, message: Message):
        bot = session.query(models.Bot).filter_by(token=self.bot.token).one()
        q = session.query(models.User).filter_by(chat_id=message.chat.id)
        logger.info(f'{q.count()}')
        if not q.count():
            user = models.User(chat_id=message.chat.id, username=message.chat.username,
                               first_name=message.chat.first_name,
                               last_name=message.chat.last_name)
            session.add(user)
        else:
            user = q.one()
            if user.bot_ban:
                user.bot_ban = False
        text = 'Меню Адміністратора' if models.is_admin(bot, user) else 'Меню'
        self.send_message(chat_id=message.chat.id,
                          text=text,
                          reply_markup=admin_menu_markup() if models.is_admin(bot, user) else menu_markup(),
                          parse_mode="Markdown")

    @_except_errors()
    @_get_session()
    def _reboot(self, session: sql_Session, message: Message):
        bot = session.query(models.Bot).filter_by(token=self.bot.token).one()
        q = session.query(models.User).filter_by(chat_id=message.chat.id)
        if not q.count():
            # raise Exception('Вы не зарегистрованы в этом боте')
            return
        user = q.one()
        if models.is_admin(bot, user):
            text = 'Бот перезапуститься в 10 секунд'
            self.send_message(message.chat.id, text)
            os.abort()

    @register_next_step_handlers_callback()
    @_except_errors()
    def _post_message_next_step(self, message: Message):
        # print(message)
        if message.content_type == 'media':
            self.mailing_post.text = message.html_caption
            self.mailing_post.type = 'media'
            self.mailing_post.file_id = message.media
        elif message.photo:
            self.mailing_post.text = message.html_caption
            self.mailing_post.type = 'photo'
            self.mailing_post.file_id = message.photo[-1].file_id
        elif message.video:
            self.mailing_post.text = message.html_caption
            self.mailing_post.type = 'video'
            self.mailing_post.file_id = message.video.file_id
        elif message.document:
            self.mailing_post.text = message.html_caption
            self.mailing_post.type = 'document'
            self.mailing_post.file_id = message.document.file_id
        elif message.text:
            self.mailing_post.text = message.html_text
            self.mailing_post.type = 'text'
        text = 'Выберите действие'
        self.send_message(message.chat.id, text, reply_markup=post_message_configuration_markup())

    @register_next_step_handlers_callback()
    @_except_errors()
    def _post_message_attach_media(self, message: Message):
        if message.media_group_id and message.photo:
            self.mailing_post.type = 'media'
            self.mailing_post.file_id = message.media
        elif message.photo:
            self.mailing_post.type = 'photo'
            self.mailing_post.file_id = message.photo[-1].file_id
        elif message.video:
            self.mailing_post.type = 'video'
            self.mailing_post.file_id = message.video.file_id
        elif message.document:
            self.mailing_post.type = 'document'
            self.mailing_post.file_id = message.document.file_id
        else:
            self.send_message(message.chat.id, 'Ошибка')
            text = 'Оберіть дію'
            self.send_message(message.chat.id, text, reply_markup=post_message_configuration_markup())
        text = 'Медіа-файл успішно прикріплено. ' \
               'Оберіть наступну дію'
        self.send_message(message.chat.id, text, reply_markup=post_message_configuration_markup())

    @_get_session()
    def _notify_next_handlers(self, session: sql_Session, new_messages: List[Message]):
        i = 0
        while i < len(new_messages):
            message = new_messages[i]
            chat_id = message.chat.id
            was_poped = False

            q = session.query(models.NextStepHandler).filter_by(chat_id=chat_id)
            if q.count():
                next_step_handler = q.one()
                handler_callback = self.next_step_handlers_callback[next_step_handler.callback]
                args = []
                for arg in next_step_handler.args:
                    args.append(arg.__getattribute__(arg.type))
                session.delete(next_step_handler)
                session.commit()
                if next_step_handler.callback in ['new_photo_in_slider', 'new_command_handler2', 'add_users_handler']:
                    handler_callback(message, *args)
                else:
                    self._exec_task(handler_callback, message, *args)
                new_messages.pop(i)  # removing message that detects with next_step_handler
                was_poped = True
            if not was_poped:
                i += 1

    @_get_session()
    def register_next_step_handler_by_chat_id(self, session: sql_Session, chat_id: int, callback, *args, **kwargs):
        q = session.query(models.NextStepHandler).filter_by(chat_id=chat_id)
        if q.count():
            next_step_handler = q.one()
            session.delete(next_step_handler)
        next_step_handler = models.NextStepHandler(chat_id=chat_id, callback=callback.__name__)
        for arg in args:
            if type(arg) is str:
                _arg = models.Arg(type='str', str=arg)
            elif type(arg) is int:
                _arg = models.Arg(type='int', int=arg)
            elif type(arg) is bool:
                _arg = models.Arg(type='bool', bool=arg)
            elif arg is None:
                _arg = models.Arg(type='str')
            else:
                raise TypeError('Підтримуються тільки str, bool, int')
            next_step_handler.args.append(_arg)

        session.add(next_step_handler)

    @_get_session()
    def clear_step_handler_by_chat_id(self, session: sql_Session, chat_id: int):
        q = session.query(models.NextStepHandler).filter_by(chat_id=chat_id)
        if q.count():
            next_step_handler = q.one()
            session.delete(next_step_handler)

    def process_new_messages(self, new_messages: List[Message]):
        new_messages_info = ''
        for message in new_messages:
            new_messages_info += f'\n{message}'
        logger.info(f'New messages: {new_messages_info}')
        self._notify_next_handlers(new_messages)
        self._notify_reply_handlers(new_messages)
        self.__notify_update(new_messages)
        self._notify_command_handlers(self.message_handlers, new_messages)

    def callback_query_decorator(self):
        def decorator(func):
            @self.callback_query_handler(func=lambda call: True)
            @self.except_errors(is_call=1)
            @self.get_session()
            def wrapper(session: sql_Session, call: CallbackQuery):
                if call.message.chat.type != 'private':
                    return
                message = call.message
                q = session.query(models.User).filter_by(chat_id=message.chat.id)
                if not q.count():
                    return
                    # raise Exception('Вы не зарегистрованы в этом боте')
                user = q.one()
                cdata = call.data.split('__')
                logger.info(
                    f'Callback Query: {user.first_name} {user.last_name or ""} ({user.username or user.chat_id}) -> {cdata}'.replace(
                        '  ', ' '))

                # ~start~
                # Обработчик клика на inline-кнопку
                clear_step_handler = func(session, call, message, cdata, user)

                if clear_step_handler is None:
                    self.callback_inline_default(session=session, call=call, cdata=cdata, user=user)

                elif clear_step_handler:
                    self.clear_step_handler(message)

            return wrapper

        return decorator

    def callback_inline_default(self, session: sql_Session, call: CallbackQuery, cdata: list,
                                user: models.User):
        clear_step_handler = False
        message = call.message

        bot = session.query(models.Bot).filter_by(token=self.bot.token).one()
        if cdata[0] == 'return_to_start_menu':
            if models.is_admin(bot, user):
                text = 'Меню Адміністратора'
                send_markup = admin_menu_markup()
            else:
                text = 'Основне меню'
                send_markup = menu_markup()
            if len(cdata) > 1:
                self.delete_message(message.chat.id, message.message_id)
                self.send_message(message.chat.id, text=text,
                                  reply_markup=send_markup,
                                  parse_mode="Markdown")
            else:
                self.edit_message_text(message=message,
                                       text=text,
                                       reply_markup=send_markup,
                                       parse_mode="Markdown")
        elif cdata[0] == 'statistics':
            count = session.query(models.User).count()
            count_ban = session.query(models.User).filter_by(bot_ban=True).count()
            count_admin = session.query(models.UserBotAssociation).filter_by(is_admin=True,
                                                                             bots_id=self.get_current_bot().id).count()
            one_day_ago = datetime.datetime.utcnow() - datetime.timedelta(days=1)

            count_new_users = session.query(models.User).filter(models.User.added >= one_day_ago).count()
            text = f'Всього користувачів: {count}\nНових за останні 24 години: {count_new_users}' \
                   f'\nЗаблокували бот: {count_ban}\nАдміністраторів: {count_admin}'
            self.edit_message_text(message,
                                   text=text, reply_markup=return_markup())
        elif cdata[0] == 'get_db':
            if models.is_admin(self.get_current_bot(), user):
                with open('telegram_bot.db', 'rb') as f:
                    self.send_document(user.chat_id, f)
                self.delete_message(chat_id=message.chat.id, message_id=message.message_id)
                text = 'Меню адміністратора'
                self.send_message(
                    chat_id=user.chat_id,
                    text=text, reply_markup=admin_menu_markup())

        # MailingPost
        if cdata[0] == 'mailing':
            print(self.mailing_post)
            if len(cdata) < 2:
                text = f'Надішліть боту повідомлення, яке бажаєте розіслати усім користувачам бота'
                self.mailing_post = models.MailingPost()
                self.send_message(chat_id=message.chat.id, text=text)
                self.delete_message(chat_id=message.chat.id, message_id=message.message_id)
                self.clear_step_handler(message)
                clear_step_handler = False
                self.register_next_step_handler(call.message, self._post_message_next_step)


            elif cdata[1] == 'publish':
                users = session.query(models.User).filter(models.User.bot_ban.isnot(True)).all()
                self.edit_message_text(message,
                                       text='Розсилка почалась',
                                       reply_markup=return_markup('return_to_start_menu'))
                all_count_users = len(users)
                count_ = 0
                count_blocked = 0
                for i, user in enumerate(users):
                    if i % 20 == 0:
                        sleep(1)
                    try:
                        if self.mailing_post.type == 'photo':
                            self.send_photo(user.chat_id, photo=self.mailing_post.file_id,
                                            caption=self.mailing_post.text,
                                            parse_mode=self.mailing_post.parse_mode)
                        elif self.mailing_post.type == 'video':
                            self.send_video(user.chat_id, data=self.mailing_post.file_id,
                                            caption=self.mailing_post.text,
                                            parse_mode=self.mailing_post.parse_mode)
                        elif self.mailing_post.type == 'document':
                            self.send_document(user.chat_id, data=self.mailing_post.file_id,
                                               caption=self.mailing_post.text,
                                               parse_mode=self.mailing_post.parse_mode)
                        elif self.mailing_post.type == 'media':
                            self.send_media_group(chat_id=user.chat_id, media=self.mailing_post.file_id)
                        else:
                            self.send_message(user.chat_id, text=self.mailing_post.text,
                                              parse_mode=self.mailing_post.parse_mode,
                                              disable_web_page_preview=self.mailing_post.disable_web_page_preview)
                        count_ += 1
                        logger.info(f'({count_}/{all_count_users}) Розсилка відправлена користувачу - {user}')
                    except Exception as e:
                        if 'bot was blocked by the user' in str(e):
                            user.bot_ban = True
                            count_blocked += 1
                            logger.info(
                                f'({count_}/{all_count_users}) Розсилка не відправлена користувачу - {user},'
                                f' користувач заблокував бота')
                        logger.info(f'({count_}/{all_count_users}) Розсилка не відправлена користувачу - {user}, '
                                    f'Помилка: {e}')
                self.send_message(chat_id=message.chat.id,
                                  text=f'Відправлено {count_} із {all_count_users}\n'
                                       f'Із них заблокували бота - {count_blocked}',
                                  reply_markup=return_markup('return_to_start_menu'))
            elif cdata[1] == 'attach_media':
                text = 'Надішліть боту медіфайл (зображення, кілька зображень, відео, GIF)'
                self.send_message(chat_id=message.chat.id, text=text)
                self.delete_message(chat_id=message.chat.id, message_id=message.message_id)
                self.clear_step_handler(message)
                clear_step_handler = False
                self.register_next_step_handler(call.message, self._post_message_attach_media)

            elif cdata[1] == 'clear':
                text = f'Надішліть боту повідомлення, яке бажаєте розіслати усім користувачам бота'
                self.send_message(chat_id=message.chat.id, text=text, parse_mode='HTML')
                self.delete_message(chat_id=message.chat.id, message_id=message.message_id)
                self.clear_step_handler(message)
                clear_step_handler = False
                self.register_next_step_handler(call.message, self._post_message_next_step)

            elif cdata[1] == 'preview':
                self.delete_message(chat_id=message.chat.id, message_id=message.message_id)
                try:
                    if self.mailing_post.type == 'text':
                        self.send_message(chat_id=message.chat.id, text=self.mailing_post.text,
                                          parse_mode=self.mailing_post.parse_mode,
                                          disable_web_page_preview=self.mailing_post.disable_web_page_preview)
                    elif self.mailing_post.type == 'photo':
                        self.send_photo(chat_id=message.chat.id, photo=self.mailing_post.file_id,
                                        caption=self.mailing_post.text,
                                        parse_mode=self.mailing_post.parse_mode)
                    elif self.mailing_post.type == 'video':
                        self.send_video(chat_id=message.chat.id, data=self.mailing_post.file_id,
                                        caption=self.mailing_post.text,
                                        parse_mode=self.mailing_post.parse_mode)
                    elif self.mailing_post.type == 'document':
                        self.send_document(chat_id=message.chat.id, data=self.mailing_post.file_id,
                                           caption=self.mailing_post.text,
                                           parse_mode=self.mailing_post.parse_mode)
                    elif self.mailing_post.type == 'media':
                        self.send_media_group(chat_id=message.chat.id, media=self.mailing_post.file_id)
                    text = 'Выберите действие'
                    self.send_message(message.chat.id, text,
                                      reply_markup=post_message_configuration_markup())
                except Exception as e:
                    raise e

        if clear_step_handler:
            self.clear_step_handler(message)

    def get_current_bot(self):
        return models.session.query(models.Bot).filter_by(token=self.token).one()

    def send_copy_message(self, message: Message, chat_id: int, disable_web_page_preview: Optional[bool] = None,
                          disable_notification: Optional[bool] = None,
                          reply_markup: Optional[Union[InlineKeyboardMarkup, ReplyKeyboardMarkup]] = None):
        if message.content_type == 'text':
            self.send_message(chat_id=chat_id, text=message.html_text,
                              parse_mode='html',
                              disable_web_page_preview=disable_web_page_preview,
                              disable_notification=disable_notification,
                              reply_markup=reply_markup)
        elif message.content_type == 'photo':
            self.send_photo(chat_id=chat_id, photo=message.photo[-1].file_id,
                            caption=message.html_caption,
                            parse_mode='html',
                            disable_notification=disable_notification,
                            reply_markup=reply_markup)
        elif message.content_type == 'video':
            self.send_video(chat_id=chat_id, data=message.video.file_id,
                            caption=message.html_caption,
                            parse_mode='html',
                            disable_notification=disable_notification,
                            reply_markup=reply_markup)
        elif message.content_type == 'document':
            self.send_document(chat_id=chat_id, data=message.document.file_id,
                               caption=message.html_caption,
                               parse_mode='html',
                               disable_notification=disable_notification,
                               reply_markup=reply_markup)
        elif message.content_type == 'voice':
            self.send_voice(chat_id=chat_id, voice=message.voice.file_id,
                            caption=message.html_caption,
                            parse_mode='html',
                            disable_notification=disable_notification,
                            reply_markup=reply_markup)
        elif message.content_type == 'video_note':
            self.send_video_note(chat_id=chat_id, data=message.voice.file_id,
                                 disable_notification=disable_notification,
                                 reply_markup=reply_markup)
        elif message.content_type == 'media':
            self.send_media_group(chat_id=chat_id, media=message.media)
        else:
            raise Exception('this type of message is not supported')

    def send_large_text(self, chat_id, text, disable_web_page_preview=None, reply_to_message_id=None, reply_markup=None,
                        parse_mode=None, disable_notification=None):
        splitted_text = util.split_string(text, 4096)
        for text in splitted_text:
            self.send_message(chat_id=chat_id, text=text, disable_web_page_preview=disable_web_page_preview,
                              reply_to_message_id=reply_to_message_id, reply_markup=reply_markup,
                              parse_mode=parse_mode, disable_notification=disable_notification)


class Bot:
    def __init__(self, _token):
        self.token = _token
        self.bot = TeleBot(self.token, session_engine=models.engine, num_threads=1)

        # ~start~
        def send_message(chat_id: int, message: models.MessageBot,
                         reply_markup: Union[InlineKeyboardMarkup, ReplyKeyboardMarkup] = None):
            if message.content_type == 'text':
                self.bot.send_message(chat_id, text=message.text, parse_mode='html', reply_markup=reply_markup)
            elif message.content_type == 'photo':
                self.bot.send_photo(chat_id, photo=message.file_id, caption=message.text, parse_mode='html',
                                    reply_markup=reply_markup)
            elif message.content_type == 'video':
                self.bot.send_video(chat_id, data=message.file_id, caption=message.text, parse_mode='html',
                                    reply_markup=reply_markup)
            elif message.content_type == 'document':
                self.bot.send_document(chat_id, data=message.file_id, caption=message.text, parse_mode='html',
                                       reply_markup=reply_markup)
            elif message.content_type == 'voice':
                self.bot.send_voice(chat_id, voice=message.file_id, caption=message.text, parse_mode='html',
                                    reply_markup=reply_markup)
            elif message.content_type == 'video_note':
                self.bot.send_video_note(chat_id, data=message.file_id, reply_markup=reply_markup)
            elif message.content_type == 'slider':
                self.bot.send_photo(chat_id, photo=message.slider_messages[0].file_id,
                                    caption=message.slider_messages[0].text,
                                    parse_mode='html',
                                    reply_markup=generate_slider_markup(0, message.id, len(message.slider_messages)))

        def send_messages_command(chat_id: int, user: models.User, command: Command):
            i = 0
            for message_bot in command.messages:
                if i == 0 and command.buttons:
                    if message_bot.content_type == 'slider':
                        self.bot.send_message(chat_id, 'ᅠ',
                                              reply_markup=generate_markup(user, command.buttons,
                                                                           command.one_time_keyboard))
                        send_message(chat_id, message_bot)
                    else:
                        send_message(chat_id, message_bot,
                                     reply_markup=generate_markup(user, command.buttons, command.one_time_keyboard))
                else:
                    send_message(chat_id, message_bot)
                i += 1

        @self.bot.message_handler(
            func=lambda m: m.chat.type == 'private' and m.reply_to_message,
            content_types=['voice', 'document', 'text', 'location', 'contact', 'sticker', 'photo', 'video',
                           'video_note'])
        @utils.except_errors()
        def reply_handler(message: Message):
            if message.reply_to_message.from_user.id == self.bot.me.id:
                self.bot.send_copy_message(message, message.chat.id)

        @self.bot.register_next_step_handlers_callback()
        @self.bot.get_session()
        def new_photo_in_slider(session: sql_Session, message: Message, message_bot_id: int, flag: int = 1):
            q = session.query(models.MessageBot).filter_by(id=message_bot_id)
            if not q.count():
                self.bot.send_message(message.chat.id, 'Очікувалось зображення, повторіть знову')
                raise Exception('Очікувалось зображення, повторіть знову')
            else:
                message_bot = q.one()
                command_name = message_bot.command.name
                if message.text == 'Відміна':
                    session.delete(message_bot)
                    self.bot.send_message(message.chat.id, 'Створення слайдера відмінено.')

                    self.bot.send_message(message.chat.id,
                                          "Відправте боту усе, що бажаєте додати до команди та натисніть 'Зберегти'.",
                                          reply_markup=new_command_menu_markup() if flag == 1 else next_change_command_messages_markup())
                    self.bot.register_next_step_handler(message, new_command_handler2, command_name)
                elif message.text == 'Зберегти':
                    if not message_bot.slider_messages:
                        text = 'Для створення слайдера Ви повинні надіслати одне або кілька повідомлень, які стануть сторінками ' \
                               'слайдера. Ви можете надіслати картинку з текстом чи без.\n\n' \
                               'Перед тим як зберегти команду, дочекайтесь повної відправки усіх повідомлень.'
                        self.bot.send_message(message.chat.id, text=text, reply_markup=new_slider_markup())
                        self.bot.register_next_step_handler(message, new_photo_in_slider, message_bot_id, flag)
                    else:
                        self.bot.send_message(message.chat.id, 'Слайдер додано.')
                        send_message(message.chat.id, message_bot)
                        self.bot.send_message(message.chat.id,
                                              "Надішліть боту все, що хочете додати до команди та натисніть 'Зберегти'.",
                                              reply_markup=new_command_menu_markup() if flag == 1 else next_change_command_messages_markup())
                        self.bot.register_next_step_handler(message, new_command_handler2, command_name, flag)

                elif message.content_type == 'photo':
                    message_bot.slider_messages.append(
                        models.SliderMessage(text=message.html_caption, file_id=message.photo[-1].file_id)
                    )
                    self.bot.register_next_step_handler(message, new_photo_in_slider, message_bot_id, flag)
                else:
                    self.bot.send_message(message.chat.id, 'Очікувалось зображення, повторіть знову')
                    raise Exception('Очікувалось зображення, повторіть знову')

        @self.bot.register_next_step_handlers_callback()
        @self.bot.get_session()
        def new_command_handler2(session: sql_Session, message: Message, command_name: str, flag: int = 1):
            q = session.query(Command).filter_by(name=command_name)
            if not q.count():
                raise Exception('Формат не підтримується')
            else:
                command = q.one()
                if message.text == 'Відміна' and flag == 1:
                    session.delete(command)
                    self.bot.send_message(message.chat.id, 'Створення команди відмінено.')
                    send_change_menu_markup(session, message.chat.id)

                elif message.text == 'Зберегти':
                    if not command.messages:
                        text = 'Для створення команди Ви повинні надіслати одне або кілька повідомлень, які стануть відповідями на ' \
                               'команду. Ви можете надіслати текст, зображення, музику або інші файли.\n\n' \
                               'Перед тим, як зберігати команду, дочекайтесь повної відправки усіх повідомлень та завантаження' \
                               'усіх доданих файлів.'
                        self.bot.send_message(message.chat.id, text=text,
                                              reply_markup=new_command_menu_markup() if flag == 1 else next_change_command_messages_markup())
                        self.bot.register_next_step_handler(message, new_command_handler2, command_name, flag)
                    else:
                        command.active = True
                        self.bot.send_message(message.chat.id,
                                              f'Команда /{command_name} успішно створена.' if flag == 1 else 'Нові повідомлення'
                                                                                                             ' успішно додано')
                        if flag == 1:
                            send_change_menu_markup(session, message.chat.id)
                        else:
                            send_change_command_menu(message, command.name)
                elif message.text == 'Додати слайдер':
                    self.bot.send_message(message.chat.id,
                                          text="Надішліть кілька повідомлень з зображеннями та текст(необов'язково), "
                                               "які будуть в слайдері", reply_markup=new_slider_markup())

                    message_bot = models.MessageBot(content_type='slider')
                    command.messages.append(message_bot)
                    session.commit()
                    self.bot.register_next_step_handler(message, new_photo_in_slider, message_bot.id, flag)
                elif message.content_type in ['text', 'photo', 'video', 'document', 'voice', 'video_note']:
                    message_bot = models.MessageBot(content_type=message.content_type,
                                                    text=message.html_text or message.html_caption)
                    if message.content_type in ['video', 'document', 'voice', 'video_note']:
                        message_bot.file_id = message.__getattribute__(message.content_type).file_id
                    elif message.content_type == 'photo':
                        message_bot.file_id = message.photo[-1].file_id
                    command.messages.append(message_bot)
                    self.bot.register_next_step_handler(message, new_command_handler2, command_name, flag)
                else:
                    raise Exception('Формат не підтримується')

        @self.bot.register_next_step_handlers_callback()
        @self.bot.get_session()
        def new_command_handler(session: sql_Session, message: Message):
            if message.text == 'Відміна':
                self.bot.send_message(message.chat.id, 'Створення команди відмінено.')
                send_change_menu_markup(session, message.chat.id)
                return

            command_name = message.text.lstrip('/')

            if command_name == re.findall(r'[A-Za-z0-9_]+', command_name)[0]:
                q = session.query(Command).filter_by(name=command_name)
                if q.count():
                    text = "Така команда вже існує. Будь ласка, оберіть іншу назву команди."
                    self.bot.send_message(message.chat.id, text)
                    self.bot.register_next_step_handler(message, new_command_handler)
                else:
                    text = "У відповідь на користувацьку команду бот може відповісти одним або кількома повідомленнями," \
                           " у тому числі будь-якими файлами, музикою, зображеннями і таке інше.\n\n" \
                           "Надішліть боту усе, що бажаєте додати до команди та натисніть 'Зберегти'."
                    self.bot.send_message(message.chat.id, text, reply_markup=new_command_menu_markup())
                    session.add(Command(name=command_name))
                    self.bot.register_next_step_handler(message, new_command_handler2, command_name)
            else:
                text = "Команда має містити тільки латинські літери, цифри та '_'.\n\n" \
                       "Наприклад:\n/faq\n/chat_link\n/photos\n\nСпробуйте ще раз."
                self.bot.send_message(message.chat.id, text)
                self.bot.register_next_step_handler(message, new_command_handler)

        @self.bot.register_next_step_handlers_callback()
        @self.bot.get_session()
        def choice_button_name_handler(session: sql_Session, message: Message, command_name: str, _command_name: str):
            if message.text:
                q = session.query(Command).filter_by(name=command_name)
                if q.count():
                    command = q.one()
                    command.free = False
                    command.button = MessageButton(text=message.text, _command_name=_command_name)
                    self.bot.send_message(message.chat.id, 'Команда успешно додана в меню.')
                    message_buttons = session.query(MessageButton).filter_by(_command_name=_command_name).all()
                    send_change_gl_menu(message, message_buttons, _command_name)

            else:
                raise Exception()

        @self.bot.register_next_step_handlers_callback()
        @self.bot.get_session()
        def choice_command_handler(session: sql_Session, message: Message, _command_name: str):
            if message.text == 'Назад':
                # send_change_menu_markup(session=session, chat_id=message.chat.id)
                message_buttons = session.query(MessageButton).filter_by(_command_name=_command_name).all()
                send_change_gl_menu(message, message_buttons, _command_name)

            elif util.is_command(message.text):
                command = session.query(Command).filter_by(name=message.text.lstrip('/'),
                                                           active=True, free=True).one()
                self.bot.send_message(message.chat.id,
                                      'Введіть назву для нового пункта меню.')
                self.bot.register_next_step_handler(message, choice_button_name_handler, command.name, _command_name)

        def send_change_gl_menu(message, buttons, _command_name):
            text = 'Ви можете налаштувати зовнішній вигляд користувацього меню. Оберіть елемент, щоб ' \
                   ' переіменувати або видалити його.'
            self.bot.send_message(message.chat.id, text, reply_markup=change_gl_menu_markup(buttons))
            self.bot.register_next_step_handler(message, change_gl_menu_handler, _command_name)

        @self.bot.register_next_step_handlers_callback()
        @self.bot.get_session()
        def rename_menu(session: sql_Session, message: Message, button_id: int, _command_name: str):
            q = session.query(MessageButton).filter_by(id=button_id)
            if q.count():
                button = q.one()
                button.text = message.text
                self.bot.send_message(message.chat.id, 'Команда успішно переіменована.')
                message_buttons = session.query(MessageButton).filter_by(_command_name=_command_name).all()
                send_change_gl_menu(message, message_buttons, _command_name)

        @self.bot.register_next_step_handlers_callback()
        @self.bot.get_session()
        def button_menu_handler(session: sql_Session, message: Message, button_id: int, _command_name: str):
            q = session.query(MessageButton).filter_by(id=button_id)
            if not q.count():
                raise Exception()
            button = q.one()
            if message.text == button.text:
                send_change_gl_menu(message, button.command.buttons, button.command.name)
            elif message.text.startswith('Режим меню: '):
                button.command.one_time_keyboard = False if button.command.one_time_keyboard else True
                text = f'Готово!\n\nПоточний режим меню: {"Зникаюче" if button.command.one_time_keyboard else "Закріплене"}.' \
                       f' Після вибору цього пункта меню користувач повернуться в Головне меню.'
                self.bot.send_message(message.chat.id, text)
                message_buttons = session.query(MessageButton).filter_by(_command_name=_command_name).all()
                send_change_gl_menu(message, message_buttons, _command_name)
            elif message.text == 'Відміна':
                message_buttons = session.query(MessageButton).filter_by(_command_name=_command_name).all()
                send_change_gl_menu(message, message_buttons, _command_name)
            elif message.text == 'Переіменувати пункт меню':
                text = f'Введіть назву, під якою команда /{button.command.name} буде відображатись в користувацькому меню.\n\n' \
                       'Тут ви можете використовувати російську, англійську або будь-яку іншу мову, цифри, символи и також emoji ✌️\n\n' \
                       f'Попередня назва команди в меню \'{button.text}\''
                self.bot.send_message(message.chat.id, text, reply_markup=ReplyKeyboardRemove())
                self.bot.register_next_step_handler(message, rename_menu, button.id, _command_name)
            elif message.text == 'Прибрати цей пункт з меню':
                text = 'Команда прибрана з користувацького меню.'
                session.delete(button)
                self.bot.send_message(message.chat.id, text)
                message_buttons = session.query(MessageButton).filter_by(_command_name=_command_name).all()
                send_change_gl_menu(message, message_buttons, _command_name)
            else:
                raise Exception()

        @self.bot.register_next_step_handlers_callback()
        @self.bot.get_session()
        def change_gl_menu_handler(_session: sql_Session, message: Message, _command_name: str = None):
            if util.is_command(message.text):
                if message.text.startswith('/start'):
                    return start(message)
                if message.text.startswith('/admins'):
                    return self.bot._create_admin(_session, message)
            if message.text == '➕ Додати пункт меню ➕':
                commands = _session.query(Command).filter_by(active=True, free=True).all()
                self.bot.send_message(message.chat.id,
                                      'Оберіть із списку доступних команд ту, яку Ви бажаєте додати в меню.',
                                      reply_markup=choice_command_markup(commands))
                self.bot.register_next_step_handler(message, choice_command_handler, _command_name)

                return
            elif message.text == 'Назад':
                if not _command_name:
                    send_change_menu_markup(session=_session, chat_id=message.chat.id)
                else:
                    q = _session.query(Command).filter_by(name=_command_name)
                    if q.count():
                        command = q.one()
                        message_buttons = _session.query(MessageButton).filter_by(command_name=command.name).all()
                        send_change_gl_menu(message, message_buttons, message_buttons[0]._command_name)
            else:
                q = _session.query(MessageButton).filter_by(text=message.text)
                if q.count():
                    button = q.one()
                    self.bot.send_message(message.chat.id,
                                          'Оберіть дію',
                                          reply_markup=button_menu_markup(button.text,
                                                                          button.command.one_time_keyboard))
                    self.bot.register_next_step_handler(message, button_menu_handler, button.id, _command_name)
                else:
                    raise Exception()

        @self.bot.register_next_step_handlers_callback()
        @self.bot.get_session()
        def delete_message_bot_yes_or_not_handler(session: sql_Session, message: Message, message_bot_id: int):
            q = session.query(models.MessageBot).filter_by(id=message_bot_id)
            if not q.count():
                raise Exception()
            message_bot = q.one()
            if message.text == 'Відміна':
                self.bot.send_message(message.chat.id, 'Видалення відмінено.',
                                      reply_markup=next_change_command_markup())
                self.bot.register_next_step_handler(message, next_change_command, message_bot.command_name)
            elif message.text == 'Видалити повідомлення':
                command_name = message_bot.command_name
                session.delete(message_bot)
                self.bot.send_message(message.chat.id, 'Повідомлення успішно видалено.',
                                      reply_markup=next_change_command_markup())
                self.bot.register_next_step_handler(message, next_change_command, command_name)
            else:
                raise Exception()

        @self.bot.register_next_step_handlers_callback()
        @self.bot.get_session()
        def delete_all_messages_yes_or_not_handler(session: sql_Session, message: Message, command_name: str):
            q = session.query(Command).filter_by(name=command_name)
            if not q.count():
                raise Exception()
            command = q.one()
            if message.text == 'Відміна':
                self.bot.send_message(message.chat.id, 'Видалення відмінено.',
                                      reply_markup=next_change_command_markup())
                self.bot.register_next_step_handler(message, next_change_command, command_name)
            elif message.text == 'Видалити усі повідомлення':
                for message_bot in command.messages:
                    session.delete(message_bot)
                command.active = False
                session.commit()
                self.bot.send_message(message.chat.id, 'Повідомлення були успішно видалені.\n\n'
                                                       'Тепер ви повинні додати хоча б одне повідомлення.',
                                      )
                text = "Відправте боту все, що хочете додати до вже існуючих відповідей на команду та натисніть 'Зберігти'."
                self.bot.send_message(message.chat.id, text, reply_markup=next_change_command_messages_markup())
                self.bot.register_next_step_handler(message, new_command_handler2, command_name, 2)
            else:
                raise Exception()

        @self.bot.register_next_step_handlers_callback()
        @self.bot.get_session()
        def delete_command_yes_or_not_handler(session: sql_Session, message: Message, command_name: str):
            q = session.query(Command).filter_by(name=command_name)
            if not q.count():
                raise Exception()
            command = q.one()
            if message.text == 'Відміна':
                self.bot.send_message(message.chat.id, 'Видалення відмінено.',
                                      reply_markup=next_change_command_markup())
                self.bot.register_next_step_handler(message, next_change_command, command_name)
            elif message.text == 'Видалити команду':
                session.delete(command)
                session.commit()
                self.bot.send_message(message.chat.id, f'Команда /{command_name} успішно видалена.',
                                      )
                send_change_menu_markup(session, message.chat.id)
            else:
                raise Exception()

        @self.bot.register_next_step_handlers_callback()
        @self.bot.get_session()
        def next_change_command(session: sql_Session, message: Message, command_name: str):
            q = session.query(Command).filter_by(name=command_name)
            if not q.count():
                raise Exception()
            command = q.one()
            if message.text == 'Назад':
                send_change_command_menu(message, command.name)
            elif util.is_command(message.text):
                message_bot_id = int(message.text.split('delete')[-1])
                q = session.query(models.MessageBot).filter_by(id=message_bot_id)
                if q.count():
                    message_bot = q.one()
                    self.bot.send_message(message.chat.id, 'Ви дійсно бажаєте видалити це повідомлення?',
                                          reply_markup=delete_message_bot_yes_or_not_markup())
                    send_message(message.chat.id, message_bot)
                    self.bot.register_next_step_handler(message, delete_message_bot_yes_or_not_handler, message_bot_id)
            elif message.text == 'Видалити усі повідомлення':
                self.bot.send_message(message.chat.id, 'Ви дійсно бажаєте видалити усі повідомлення?',
                                      reply_markup=delete_all_messages_yes_or_not_markup())
                self.bot.register_next_step_handler(message, delete_all_messages_yes_or_not_handler, command_name)
            else:
                raise Exception()

        @self.bot.register_next_step_handlers_callback()
        @self.bot.get_session()
        def access_command_handler(session: sql_Session, message: Message, command_name: str):
            q = session.query(Command).filter_by(name=command_name)
            if not q.count():
                raise Exception()
            command = q.one()
            if message.text == 'Назад':
                send_change_command_menu(message, command.name)
            elif message.text:
                message_text = message.text.lstrip('✔️ ')
                if message.text.startswith('✔️ '):
                    if message_text == 'Усі користувачі':
                        command.groups_type = 'added'
                    elif message_text == 'Усі учасники груп':
                        command.groups_type = 'added'
                    else:
                        q = session.query(models.Group).filter_by(name=message_text)
                        if q.count():
                            group = q.one()
                            command.groups.remove(group)
                        else:
                            raise Exception()

                else:
                    if message_text == 'Усі користувачі':
                        command.groups_type = 'all'
                    elif message_text == 'Усі учасники груп':
                        command.groups_type = 'all_reg'
                    else:
                        q = session.query(models.Group).filter_by(name=message_text)
                        if q.count():
                            group = q.one()
                            command.groups.append(group)
                        else:
                            raise Exception()
                send_change_access_command_menu(session, message, command)
            else:
                raise Exception()

        def send_change_access_command_menu(session, message: Message, command: Command):
            markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
            markup.row(f'✔️ Усі користувачі' if command.groups_type == 'all' else 'Усі користувачі')
            markup.row(f'✔️ Усі  учасники груп' if command.groups_type == 'all_reg' else 'Усі участники групп')
            for group in session.query(models.Group).all():
                markup.row(f'✔️ {group.name}' if group in command.groups else group.name)
            markup.row('Назад')
            self.bot.send_message(message.chat.id, f'Оберіть групи, які будуть мати доступ до команди /{command.name}.',
                                  reply_markup=markup)
            self.bot.register_next_step_handler(message, access_command_handler, command.name)

        @self.bot.register_next_step_handlers_callback()
        @self.bot.get_session()
        def change_command_handler(session: sql_Session, message: Message, command_name: str):
            q = session.query(Command).filter_by(name=command_name)
            if not q.count():
                raise Exception()
            command = q.one()
            if message.text == 'Назад':
                send_change_menu_markup(session=session, chat_id=message.chat.id)
            elif message.text == 'Показати команду':
                for message_bot in command.messages:
                    send_message(message.chat.id, message_bot)
                send_change_command_menu(message, command.name)
            elif message.text == 'Редагувати відповіді команди':
                i = 0
                for message_bot in command.messages:
                    if i == 0:
                        send_message(message.chat.id, message_bot, reply_markup=next_change_command_markup())
                    else:
                        send_message(message.chat.id, message_bot)
                    self.bot.send_message(message.chat.id,
                                          f'⬆️ Натисніть, щоб видалити це повідомлення: /{command.name}__delete{message_bot.id}')
                    i += 1
                self.bot.register_next_step_handler(message, next_change_command, command_name)
            elif message.text == 'Налаштувати доступ до команди':
                send_change_access_command_menu(session, message, command)
            elif message.text == 'Налаштувати меню команди':
                send_change_gl_menu(message, command.buttons, command.name)
            elif message.text == 'Видалити команду':
                self.bot.send_message(message.chat.id, f'Ви точно бажаєте видалити команду /{command.name}?',
                                      reply_markup=delete_command_yes_or_not_markup())
                self.bot.register_next_step_handler(message, delete_command_yes_or_not_handler, command_name)
            else:
                raise Exception()

        def send_change_command_menu(message: Message, command_name: str):
            text = f'Редагуємо команду /{command_name}.\n\n' \
                   f'Ви можете проглянути команду, видалити її або редагувати відповіді команди.'
            self.bot.send_message(message.chat.id, text, reply_markup=change_command_menu_markup())
            self.bot.register_next_step_handler(message, change_command_handler, command_name)

        @self.bot.register_next_step_handlers_callback()
        @self.bot.get_session()
        def start_change_menu_handler(session: sql_Session, message: Message):
            bot = session.query(models.Bot).filter_by(token=self.bot.token).one()
            user = session.query(models.User).filter_by(chat_id=message.chat.id).one()
            if models.is_admin(bot, user):
                if message.text == 'Створити команду':
                    text = "Вкажіть назву для нової команди. Команда може містити латинські літери, букви, цифри та '_'.\n\n" \
                           "Приклад команди:\n/website\n/pricelist\n/contacts\n/best_music\n/best_photos"
                    self.bot.send_message(user.chat_id, text, reply_markup=cancel_reply_markup())
                    self.bot.register_next_step_handler(message, new_command_handler)
                elif message.text == 'Налаштувати головне меню':
                    message_buttons = session.query(MessageButton).filter_by(_command_name=None).all()
                    send_change_gl_menu(message, message_buttons, None)
                elif util.is_command(message.text):
                    if message.text.startswith('/start'):
                        return start(message)
                    if message.text.startswith('/admins'):
                        return self.bot._create_admin(message)
                    q = session.query(Command).filter_by(name=message.text.lstrip('/'))
                    if q.count():
                        command = q.one()
                        send_change_command_menu(message, command.name)
                elif message.text == 'Назад':
                    text = 'Меню Адміністратора'
                    self.bot.send_message(chat_id=message.chat.id,
                                          text=text,
                                          reply_markup=admin_menu_markup()
                                          )
                else:
                    raise Exception()

        @self.bot.register_next_step_handlers_callback()
        def support_message_handler(message: Message):
            if message.text == 'Відміна':
                start(message, is_h=False)
            else:
                self.bot.forward_message(message.chat.id, message.chat.id, message.message_id)
                self.bot.send_message(message.chat.id, 'Очікуйте відповіді')
                start(message, is_h=False)

        @self.bot.register_next_step_handlers_callback()
        @self.bot.get_session()
        def click_handler(session: sql_Session, message: Message, _command_name: str):
            user = session.query(models.User).filter_by(chat_id=message.chat.id).one()
            if message.text:
                if message.text == 'Назад':
                    if not _command_name:
                        start(message, is_h=False)
                    else:
                        button = session.query(MessageButton).filter_by(command_name=_command_name).one()
                        command = button.command
                        is_allowed = False
                        for _group in command.groups:
                            if _group.name == user.group_name:
                                is_allowed = True
                                break
                        if is_allowed:
                            send_messages_command(chat_id=message.chat.id, user=user, command=command)
                            print(button.command_name)
                        self.bot.register_next_step_handler(message, click_handler, button.command_name)
                    return
                elif message.text == 'Допомога':
                    text = 'Ви можете задати питання викладачу.'
                    self.bot.send_message(message.chat.id, text, reply_markup=cancel_reply_markup())
                    self.bot.register_next_step_handler(message, support_message_handler)
                    return
                command = None
                if util.is_command(message.text):
                    if message.text.startswith('/start'):
                        return start(message)
                    if message.text.startswith('/admins'):
                        return self.bot._create_admin(message)
                    q = session.query(Command).filter_by(name=message.text.lstrip('/'))
                    if q.count():
                        command = q.one()
                        if not command.active:
                            command = None
                else:
                    q = session.query(MessageButton).filter_by(text=message.text)
                    if q.count():
                        button = q.one()
                        command = button.command
                if command:
                    send_messages_command(chat_id=message.chat.id, user=user, command=command)
                    self.bot.register_next_step_handler(message, click_handler,
                                                        command.button._command_name if command.buttons else _command_name)
                else:
                    raise Exception()

        @self.bot.message_handler(func=lambda m: m.chat.type == 'private', commands=['start'])
        @utils.except_errors()
        @self.bot.get_session()
        def start(session: sql_Session, message: Message, is_h: bool = True):
            bot = session.query(models.Bot).filter_by(token=self.bot.token).one()
            admin = session.query(models.UserBotAssociation).filter_by(bots_id=bot.id, is_admin=True)
            if not admin.count():
                admins = session.query(models.UserBotAssociation).filter_by(bots_id=bot.id, is_admin=True).all()
                admin = None
                for _admin in admins:
                    q = session.query(models.User).filter_by(id=_admin.user_id, chat_id=message.chat.id)
                    if q.count():
                        admin = q.one()
                if not admin:
                    user = models.User(chat_id=message.chat.id,
                                       username=message.from_user.username,
                                       first_name=message.from_user.first_name,
                                       last_name=message.from_user.last_name,
                                       added=datetime.datetime.now())
                    session.add(user)
                    session.commit()
                    sub_bot_to_user = models.UserBotAssociation(bots_id=bot.id,
                                                                user_id=user.id,
                                                                is_admin=False,
                                                                is_user_mode=False,
                                                                user=user,
                                                                bot=bot)
                    session.add(sub_bot_to_user)
                    session.commit()
            q = session.query(models.User).filter_by(chat_id=message.chat.id)
            if not q.count():
                user = models.User(chat_id=message.chat.id,
                                   username=message.from_user.username,
                                   first_name=message.from_user.first_name,
                                   last_name=message.from_user.last_name,
                                   added=datetime.datetime.now())
                session.add(user)
                session.commit()
                sub_bot_to_user = models.UserBotAssociation(bots_id=bot.id,
                                                            user_id=user.id,
                                                            is_admin=False,
                                                            is_user_mode=False,
                                                            user=user,
                                                            bot=bot)
                session.add(sub_bot_to_user)
                session.commit()

            else:
                user = q.one()
                if user.bot_ban:
                    user.bot_ban = False
            bot = session.query(models.Bot).filter_by(token=self.bot.token).one()
            if models.is_admin(bot, user) and is_h:
                text = 'Меню Адміністратора'
                self.bot.send_message(chat_id=message.chat.id,
                                      text=text,
                                      reply_markup=admin_menu_markup(),
                                      parse_mode="Markdown")
            else:
                text = 'Головне меню'
                message_buttons = session.query(MessageButton).filter_by(_command_name=None).all()
                markup = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=False)
                for button in message_buttons:
                    if models.is_admin(bot, user):
                        markup.row(button.text)
                    elif button.command.groups_type == 'all':
                        markup.row(button.text)
                    elif button.command.groups_type == 'all_reg':
                        if user.group:
                            markup.row(button.text)
                    elif user.group in button.command.groups:
                        markup.row(button.text)

                markup.row('Допомога')
                self.bot.send_message(message.chat.id, text)
                # self.bot.register_next_step_handler(message, click_handler, None)

            @self.bot.message_handler(content_types=['text', 'photo', 'video', 'document', 'voice', 'video_note'])
            @self.bot.get_session()
            def message_handler(session: sql_Session, message: Message):
                user = session.query(models.User).filter_by(chat_id=message.chat.id).one()
                if message.text:
                    command = None
                    if util.is_command(message.text):
                        q = session.query(Command).filter_by(name=message.text.lstrip('/'))
                        if q.count():
                            command = q.one()
                            if not command.active:
                                command = None
                    else:
                        q = session.query(MessageButton).filter_by(text=message.text)
                        if q.count():
                            button = q.one()
                            command = button.command
                    if command:
                        send_messages_command(chat_id=message.chat.id, user=user, command=command)

                        # send_message(message.chat.id, command.messages[0],
                        #              reply_markup=generate_slider_markup(0, command.name, len(command.messages)))

        def send_change_menu_markup(session: sql_Session, chat_id: int):
            text = 'Ви можете створювати користувацькі команди, на які ваш бот буде відповідати ' \
                   'завчасно заданим способом.\n\nВикористайте меню, щоб створити нову команду, ' \
                   'змінити зовнішній вигляд меню або оберіть конкретну команду, яку хочете налаштувати.'
            commands = session.query(Command).filter_by(active=True).all()
            self.bot.send_message(chat_id, text, reply_markup=change_menu_markup(commands))
            self.bot.register_next_step_handler_by_chat_id(chat_id, start_change_menu_handler)

        @self.bot.register_next_step_handlers_callback()
        @self.bot.get_session()
        def add_groups_handler(session: sql_Session, message: Message):
            if message.text == 'Відміна':
                self.bot.send_message(message.chat.id, 'Оберіть дію.',
                                      reply_markup=groups_markup())
                self.bot.register_next_step_handler(message, groups_menu_handlers)
            else:
                groups_name = map(str.strip, message.text.split('\n'))
                not_added = []
                for group_name in groups_name:
                    q = session.query(models.Group).filter_by(name=group_name)
                    if not q.count():
                        session.add(models.Group(name=group_name))
                    else:
                        not_added.append(group_name)
                self.bot.send_message(message.chat.id, 'Список груп оновлено.')
                self.bot.send_message(message.chat.id, 'Оберіть дію.',
                                      reply_markup=groups_markup())
                self.bot.register_next_step_handler(message, groups_menu_handlers)

        @self.bot.register_next_step_handlers_callback()
        @self.bot.get_session()
        def dell_groups_handler(session: sql_Session, message: Message):
            if message.text == 'Відміна':
                self.bot.send_message(message.chat.id, 'Оберіть дію.',
                                      reply_markup=groups_markup())
                self.bot.register_next_step_handler(message, groups_menu_handlers)
            else:
                groups_name = map(str.strip, message.text.split('\n'))
                not_delled = []
                for group_name in groups_name:
                    q = session.query(models.Group).filter_by(name=group_name)
                    if q.count():
                        group = q.one()
                        session.delete(group)
                    else:
                        not_delled.append(group_name)
                self.bot.send_message(message.chat.id, 'Список груп оновлено.')
                self.bot.send_message(message.chat.id, 'Оберіть дію.',
                                      reply_markup=groups_markup())
                self.bot.register_next_step_handler(message, groups_menu_handlers)

        @self.bot.register_next_step_handlers_callback()
        @self.bot.get_session()
        def groups_menu_handlers(session: sql_Session, message: Message):
            if message.text == 'Назад':
                self.bot._create_admin(message)
            elif message.text == 'Додати групи':
                text = 'Введіть назву груп в стовбчик\n\nПриклад:\n1група\n2група\n3група'
                self.bot.send_message(message.chat.id, text, reply_markup=cancel_reply_markup())
                self.bot.register_next_step_handler(message, add_groups_handler)
            elif message.text == 'Видалити групи':
                groups_name = []
                for group in session.query(models.Group).all():
                    groups_name.append(group.name)
                if groups_name:
                    groups_name = '\n'.join(groups_name)
                    self.bot.send_large_text(message.chat.id, f'Список групп:\n{groups_name}')
                text = 'Введіть назви груп, які Ви бажаєте видалити в стовбчик'
                self.bot.send_message(message.chat.id, text, reply_markup=cancel_reply_markup())
                self.bot.register_next_step_handler(message, dell_groups_handler)
            else:
                raise Exception()

        @self.bot.register_next_step_handlers_callback()
        @self.bot.get_session()
        def add_users_handler(session: sql_Session, message: Message, group_name: str = None):
            if message.text in ['Відміна', 'Назад']:
                self.bot.send_message(message.chat.id, 'Оберіть дію.',
                                      reply_markup=users_markup())
                self.bot.register_next_step_handler(message, users_menu_handlers)
            else:
                if group_name is None:
                    if session.query(models.Group).filter_by(name=message.text).count():
                        self.bot.send_message(message.chat.id,
                                              'Перешліть повідомлення від користувачей, які бажаєте додати',
                                              reply_markup=reply_1_markup('Назад'))
                        self.bot.register_next_step_handler(message, add_users_handler, message.text)
                    else:
                        raise Exception()
                else:
                    user = None
                    try:
                        user = session.query(models.User).filter_by(username=message.forward_from.username).one()
                        user.group_name = group_name
                    except:
                        user = models.User(group_name=group_name, chat_id=message.chat.id,
                                           username=message.forward_from.username,
                                           first_name=message.forward_from.first_name,
                                           last_name=message.forward_from.last_name, bot_ban=True)
                        session.add(user)
                        session.rollback()
                    session.commit()
                    text = f'Користувач {("@" + (user.username or "--")).replace("@--", "")} успішно доданий в групу "{group_name}".'
                    self.bot.send_message(message.chat.id, text)
                    self.bot.register_next_step_handler(message, add_users_handler, group_name)

        @self.bot.register_next_step_handlers_callback()
        @self.bot.get_session()
        def dell_users_handler(session: sql_Session, message: Message):
            if message.text == 'Відміна':
                self.bot.send_message(message.chat.id, 'Оберіть дію.',
                                      reply_markup=users_markup())
                self.bot.register_next_step_handler(message, users_menu_handlers)
            else:
                users_chat_id = map(str.strip, message.text.split('\n'))
                for user_chat_id in users_chat_id:
                    try:
                        user = session.query(models.User).filter_by(chat_id=user_chat_id)
                        session.delete(user)
                    except:
                        pass
                self.bot.send_message(message.chat.id, 'Список користувачів оновлено.')
                self.bot.send_message(message.chat.id, 'Оберіть дію.',
                                      reply_markup=users_markup())
                self.bot.register_next_step_handler(message, users_menu_handlers)

        @self.bot.register_next_step_handlers_callback()
        @self.bot.get_session()
        def users_menu_handlers(session: sql_Session, message: Message):
            if message.text == 'Назад':
                start(message)
            elif message.text == 'Додати користувачів до групи':
                groups_name = []
                for group in session.query(models.Group).all():
                    groups_name.append(group.name)
                # if groups_name:
                #     _groups_name = '\n'.join(groups_name)
                #     bot.send_large_text(message.chat.id, f'Список групп:{_groups_name}')
                text = 'Оберіть групу, в яку бажаєте додати користувачів'
                self.bot.send_message(message.chat.id, text, reply_markup=choice_groups_markup(groups_name))
                self.bot.register_next_step_handler(message, add_users_handler)
            elif message.text == 'Видалити користувачів':

                # FIXME: change if to something else
                text = 'Введіть у стовбчик id користувачів, які бажаєте видалити.\n\nПриклад:\n49340211\n36557771\n6040987'
                self.bot.send_message(message.chat.id, text, reply_markup=cancel_reply_markup())
                self.bot.register_next_step_handler(message, dell_users_handler)
            else:
                raise Exception()

        @self.bot.callback_query_decorator()
        def callback_inline(session: sql_Session, call: CallbackQuery, message: Message, cdata: List[str],
                            user: models.User):
            clear_step_handler = False
            bot = session.query(models.Bot).filter_by(token=self.bot.token).one()
            if cdata[0] == 'slider':
                q = session.query(models.MessageBot).filter_by(id=int(cdata[1]))
                if not q.count():
                    return
                message_bot = q.one()
                num_page = int(cdata[2])
                self.bot.edit_message_media(chat_id=message.chat.id, message_id=message.message_id,
                                            media=InputMediaPhoto(message_bot.slider_messages[num_page].file_id,
                                                                  caption=message_bot.slider_messages[num_page].text,
                                                                  parse_mode='html'),
                                            reply_markup=generate_slider_markup(num_page, int(cdata[1]),
                                                                                len(message_bot.slider_messages))
                                            )
            elif cdata[0] == 'pass':
                return clear_step_handler
            elif cdata[0] == 'change_commands':
                if models.is_admin(bot, user):
                    send_change_menu_markup(session=session, chat_id=message.chat.id)
            elif cdata[0] == 'users':
                self.bot.edit_message_text(message, 'Користувачі',
                                           reply_markup=users_list_markup(session.query(models.User).all()))
                self.bot.send_message(message.chat.id, 'Оберіть дію.',
                                      reply_markup=users_markup())
                self.bot.register_next_step_handler(message, users_menu_handlers)
            elif cdata[0] == 'groups':
                self.bot.send_message(message.chat.id, 'Оберіть дію.',
                                      reply_markup=groups_markup())
                self.bot.register_next_step_handler(message, groups_menu_handlers)
            elif cdata[0] == 'user_menu':
                start(message, is_h=False)
            elif cdata[0] == 'udhu':
                self.bot.answer_callback_query(call.id, 'До цього користувача відсутнє пряме посилання')
            else:
                return
            return clear_step_handler

        self.bot.delete_webhook()
        self.bot.set_webhook(url='https://bot-creator-app.herokuapp.com/' + self.token)
