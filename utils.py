from functools import wraps

from telebot.types import Message
from telebot import util

from typing import List


def check_commands(message: Message, commands: List[str]) -> bool:
    return message.content_type == 'text' and util.extract_command(message.text) in commands


def clear_markdown_text(text: str) -> str:
    chars = ['*', '_', '[', ']', '`']
    if text:
        for char in chars:
            text = text.replace(char, f'\\{char}')
        return text
    else:
        return ''


def except_errors():
    """ игнорирует ошибку """

    def decorator(func):
        @wraps(func)
        def wrapped(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except Exception as e:
                pass

        return wrapped

    return decorator
